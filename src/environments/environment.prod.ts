export const environment = {
  production: true,
  apiUrl: 'https://kyctrusts.meta1.io/dev/api',
  acUrl: '//alcyone.meta-exchange.info/dev/kyc/client/acApi',
  acUrlRenew: '/RenewToken'
};
