import { Component, OnInit, OnDestroy, OnChanges, SimpleChanges } from '@angular/core';
import { Subscription, of } from 'rxjs';
import { first, catchError } from 'rxjs/operators';
import {MatTabsModule, MatTabChangeEvent} from '@angular/material/tabs';
import {MatStepperModule} from '@angular/material/stepper';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatDialogModule} from '@angular/material/dialog';
import {MatDialog, ErrorStateMatcher} from '@angular/material';
import { MatCheckboxModule } from '@angular/material/checkbox';


import {MatDatepickerModule,MatNativeDateModule,MatIconModule} from '@angular/material';
import {
  MatButtonModule,
  MatFormFieldModule,
  MatInputModule,
  MatRippleModule } from '@angular/material';
  import { User } from '@app/_models';
  import { AlertService, UserService, AuthenticationService } from '@app/_services';
  import { Router } from '@angular/router';
  import { FormBuilder, FormGroup, Validators, FormControl, FormArray, FormGroupDirective, NgForm, ValidationErrors } from '@angular/forms';
import { DataService } from '@app/_services/data.service';
import { environment } from '@environments/environment';
// import { Countries } from '@app/_models/countries';
import { SideBarService } from '@app/_services/sidebar.service';
import { HttpClient } from '@angular/common/http';

  export interface Dessert {
    calories: number;
    carbs: number;
    fat: number;
    name: string;
    protein: number;
    status: false;
  }
  // declare var $: any;
  // class ConfirmValidParentMatcher implements ErrorStateMatcher {
  //   isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
  //       return control.parent.invalid && control.touched;
  //   }
  // }
  
/**
 * Collection of reusable error messages
 */
export const errorMessages: { [key: string]: string } = {
  'required': 'This field is required',
  'email': 'Must be Email',
  'pattern': 'Not in correct format'
};

  @Component({
    selector: 'app-nextstep',
    templateUrl: './nextstep.component.html',
    styleUrls: ['./nextstep.component.css']

  })
  export class NextstepComponent implements OnInit, OnDestroy {
    currentUser: User;
    currentUserSubscription: Subscription;
    users: User[] = [];
    verificationForm: FormGroup;
    verificationFormAdmin: FormGroup;
    verificationFormBeneficiar: FormGroup;
    verificationFormTrust: FormGroup;
    loading = false;
    submitted = false;
    step = 0;
    subStep = 1;
    public countries = [];
    public states: any[];
    isStateDropdown: boolean = false;
  tabs = ['Administrator 1', 'Administrator 2', /*'Administrator 3'*/];
    tabsBenef = ['Benefieciary 1' /*,'Benefieciar 2'  'Benefieciar 3'*/];
    selected = new FormControl(0);
    countertabs = 0;
    countertabsBenef = 0; // 2

    status: boolean;
    currentIndex = 0;
    currentIndexBenef = 0;
    public adminsArray: FormGroup[] = [];
    public bensArray: FormGroup[] = [];

    // getFormValidationErrors
    getErrors(form: FormGroup) {
      let errorsToShow: string[] = [];
      Object.keys(form.controls).forEach(key => {
    
      const controlErrors: ValidationErrors = form.get(key).errors;
      if (controlErrors != null) {
        let errorToShow: string;
            Object.keys(controlErrors).forEach(keyError => {
              const bErrorToShow: boolean = controlErrors[keyError];
              if(bErrorToShow) {
                errorToShow = keyError;
                if (errorToShow !== null && errorToShow !== undefined && errorToShow !== '' && errorMessages[errorToShow] !== undefined) {
                  errorsToShow.push(key[0].toUpperCase() + key.substr(1) + ': ' + errorMessages[errorToShow]);
                } else if (errorToShow !== null && errorToShow !== undefined && errorToShow !== '') {
                  errorsToShow.push(key[0].toUpperCase() + key.substr(1) + ': ' + errorToShow);
                }
              }
            });
          }
        });
        return errorsToShow;
      }

    public get admins(): any {
     if (this.verificationFormAdmin === null ||
      ! (this.verificationFormAdmin.controls && this.verificationFormAdmin.controls.adminsArray !== null)) {
       return null;
     } else {
        return  (this.verificationFormAdmin.controls.adminsArray as FormArray).controls;
     }
    }
    public get bens(): any {
      if (this.verificationFormBeneficiar == null ||
        ! (this.verificationFormBeneficiar.controls && this.verificationFormBeneficiar.controls.bensArray !== null)) {
        return null;
      } else {
         return ( this.verificationFormBeneficiar.controls.bensArray as FormArray ).controls;
      }
     }

    public adminArrayItems: {
      id: number;
      title: string;
    }[];

    viewModel: { isCleared: boolean } = { isCleared: false };
    constructor(
      private formBuilder: FormBuilder,
      private router: Router,
      private authenticationService: AuthenticationService,
      private userService: UserService,
      public dialog: MatDialog,
      private alertService: AlertService,
      private dataService: DataService,
      public sideBarService: SideBarService,
      private http: HttpClient
    ) {
      this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
        this.currentUser = user;
      });

      // Function to compare two objects by comparing their `unwrappedName` property.
      const compareFn = (a, b) => {
        if (a.name < b.name) {
          return -1;
        }
        if (a.name > b.name) {
          return 1;
        }
        return 0;
      };

      http.get('assets/countries.json').subscribe( (all: any[]) => {
        this.countries = all.sort(compareFn);
        this.states = Object.keys(this.countries.sort().filter(f => f.name === 'United States of America')[0].states);
      });

      this.authenticationService.alerts().subscribe(
        success => {},
        error => {}
      );
      //form build
      /*this.verificationForm = this.formBuilder.group({
        firstName: ['', Validators.required],
        lastName: ['', Validators.required],
        email: ['', [Validators.required, Validators.email]],
        // address: ['', []],
        // Address2: ['', Validators.required],
        // city: ['', [] ],
        // state: ['', [] ],
        // zip: ['', [] ],


      });*/

      // this.verificationForm.controls.firstName.setValue( this.authenticationService.currentUserValue.firstName);
      // this.verificationForm.controls.lastName.setValue( this.authenticationService.currentUserValue.lastName);
    

      this.verificationFormAdmin = this.formBuilder.group(
        {
          adminsArray: new FormArray(this.adminsArray)
        }
      );

      this.addAdmin();

      // this.adminsArray[0].controls.email.setValue( this.authenticationService.currentUserValue.email);

      this.addAdmin();

      // this.verificationFormAdmin = this.formBuilder.group(
      //   {
      //     adminsArray: new FormArray(this.adminsArray)
      //   }
      // );

      this.adminsArray[0].valueChanges.subscribe(s => {
        this.adminOnChanges(s);
       // this.adminsArray[0].valueChanges.subscribe(st =>this.adminOnChanges(st));
      });

     // this.admins = this.verificationFormAdmin.controls.adminsArray as FormArray;
    


      // this.verificationFormAdmin = this.formBuilder.group({
      //   firstName: ['',Validators.required],
      //   lastName: ['',Validators.required],
      //   Address1:['',Validators.required],
      //   Address2:['',Validators.required],
      //   state:['',Validators.required],
      //   postalCode:['',Validators.required],
      //   city:['',Validators.required],
      //   email:['',Validators.required],

      // });

      // set TrustName in OnInit

      this.addBeneficiar();

      this.verificationFormBeneficiar = this.formBuilder.group({
        bensArray: new FormArray(this.bensArray)
      });

    }
    showHint(strOrPlug, ev: any) {

      let str = '';
      switch (strOrPlug) {
        case 'TRUST': 
          str = 'Fill in your full <b>trust name</b>';
        break;
        case 'BENS':
          str = 'Fill in your full <b>benefeciaries</b> info';
        break;
        case 'ADMINS':
          str = 'Fill in your adminisitrator\'s <b>full name and address</b> info';
        break;
        default: str = strOrPlug; break;
      }

      this.sideBarService.show({data: str});
      if(ev!==undefined && ev.stopPropagation !== undefined) {
       ev.stopPropagation();
      }
    }

  onShowHint(event: MatTabChangeEvent) {
    console.log('event => ', event);
    console.log('index => ', event.index);
    console.log('tab => ', event.tab);
    let str = '';
    switch(event.tab.textLabel) {
      case 'Trust':
        str = 'TRUST'; break;
      case 'Beneficiaries':
        str = 'BENS';
      break;
      case 'Administrators':
        str = 'ADMINS';
      break;
      default: break;
    }
    this.showHint(str, {});
  }
    // validation
    /**
 * Custom ErrorStateMatcher which returns true (error exists) when the parent form group is invalid and the control has been touched
 */


/**
* Collection of reusable RegExps
*/
    adminOnChanges(changes: SimpleChanges): void {
      this.viewModel.isCleared = false;
    }

    private addBeneficiar() {
      this.bensArray.push( this.formBuilder.group({
        fullName: ['', Validators.required]
      }));
      this.countertabsBenef = this.countertabsBenef + 1;

    }
    private addAdmin() {
      // if (this.adminsArray == null) {
      //   this.adminsArray.push(this.formBuilder.array([
      // ]));

      // } else {
      //  this.adminsArray.push(this.formBuilder.group([
     //    ]));
      // }
      const control = <FormArray>this.verificationFormAdmin.controls.adminsArray;

      control.push( this.formBuilder.group({
        firstName: ['',Validators.required],
        lastName: ['',Validators.required],
        address:['',Validators.required],
        country:['',Validators.required],
        state:['',Validators.required],
        zip:['',Validators.required],
        city:['',Validators.required]
       // email:['',Validators.required]
      }));
      
      this.countertabs += 1;

      // control.valueChanges.subscribe(value => {
      //   console.log('country has changed:', value);
      //   if (value === 'United States of America') {
      //     this.states = Object.keys(this.countries.sort().filter(f => f.name === value)[0].states);
      //     this.isStateDropdown = true;
      //   } else {
      //     this.isStateDropdown = false;
      //   }
      // });

    }
    ngOnInit() {
      this.verificationFormTrust = this.formBuilder.group({
        FullName: ['', Validators.required],
      });
      // this.loadAllUsers();
   //   console.log(this.currentUser._id)
      var name: string;
      /// this.verificationForm.controls.email.setValue( this.authenticationService.currentUserValue.email);

      //const headers = {'authorization': this.authenticationService.currentUserValue.token};
      this.userService.get().subscribe( (user: any) => {
        if (user == null) {
          return;
        }
         this.userService.getAddress().subscribe( userResp => {
          console.log(user);
          // this.verificationForm.controls.firstName.setValue(userResp.firstName);
          // this.verificationForm.controls.lastName.setValue(userResp.lastName);
          // this.verificationForm.controls.email.setValue(userResp.email);
         
          this.verificationFormTrust.controls.FullName.setValue( userResp.firstName + ' ' + userResp.lastName + ' trust');
          
          this.adminsArray[0].controls.firstName.setValue( userResp.firstName !== undefined ? userResp.firstName : '' );
          this.adminsArray[0].controls.lastName.setValue( userResp.lastName !== undefined ? userResp.lastName : '' );
          // this.verificationForm.controls.address.setValue(userResp.address !== undefined ? userResp.address : '' /*userResp.address*/); // #todo
          // this.verificationForm.controls.city.setValue(userResp.city !== undefined ? userResp.city : ''/*userResp.city*/); // #todo
          // this.verificationForm.controls.state.setValue(userResp.city !== undefined ? userResp.state : '' /*userResp.state*/); // #todo
          // this.verificationForm.controls.zip.setValue(userResp.zip !== undefined ? userResp.zip : '' /*userResp.zip*/); // #todo

          // this.verificationForm.controls.email.setValue(userResp.email !== undefined && userResp.email !== 'verified' ? userResp.email : '' /*userResp.email*/); // #todo
  
          this.adminsArray[0].controls.address.setValue(userResp.address !== undefined ? userResp.address : '' /*userResp.email*/);
          this.adminsArray[0].controls.city.setValue(userResp.city !== undefined ? userResp.city : ''/*userResp.city*/);
          this.adminsArray[0].controls.state.setValue(userResp.city !== undefined ? userResp.state : '' /*userResp.state*/);
          this.adminsArray[0].controls.zip.setValue(userResp.zip !== undefined ? userResp.zip : '' /*userResp.zip*/);

          // this.adminsArray[0].controls.country.valueChanges.subscribe(v=> {
          //     if(v=='United States of America') {
          //       this.isStateDropdown = true;
          //     }
          //     else {
          //       this.isStateDropdown = false;
          //     }
          // });
          this.adminsArray[0].controls.country.setValue(userResp.country !== undefined ? userResp.country : '' /*userResp.zip*/);
        if (user.trust ==='filled') {
          this.alertService.success('Trust Submitted. Please continue next step');
        }
        name = user.trust;
        // const firstName = userResp.firstName, lastName = userResp.lastName;


        console.log(name);
        console.log(userResp);

        // if (name == "trustRejected"){
        //   this.alertService.error("Trust Rejected. Please repeat!");
        // }else if (name == "ucc1"){
        //   this.alertService.success("Trust Submitted. Download you docs.");
        //   this.status = true;
        // }else if (name == "ucc"){
        //   this.alertService.warning("Trust on check, wait!");
        //   setTimeout(function(){
        //     window.location.reload();
        //   }, 30000);
        // }else if (name == "docRejected"){
        //   this.alertService.error("Incorrect Docs. Update Again!");
        // }else if (name == "docSubmitted"){
        //   this.alertService.success("WOHO!");
        // }else{
          this.alertService.info('Enter details of your Trust in these tabs!');
        // }
      });

    });
      // this.verificationForm = this.formBuilder.group({
      //   firstName: ['', Validators.required],
      //   lastName: ['', Validators.required],
      //   Address: ['', Validators.required],
      //   Address2: ['', Validators.required],
      //   city: ['', Validators.required],
      //   state: ['', Validators.required],
      //   zip: ['', Validators.required],


      // });
      // this.verificationFormAdmin = this.formBuilder.group({
      //   firstName: ["",Validators.required],
      //   lastName: ["",Validators.required],
      //   Address1:["",Validators.required],
      //   Address2:["",Validators.required],
      //   state:["",Validators.required],
      //   postalCode:["",Validators.required],
      //   city:["",Validators.required],
      //   email:["",Validators.required],

      // });

     // this.verificationFormBeneficiar = this.formBuilder.group({
      //   FullName: ['', Validators.required],
      // });

    }

    ngOnDestroy() {
      // unsubscribe to ensure no memory leaks
      this.currentUserSubscription.unsubscribe();
    }
/*
    deleteUser(id: number) {
      this.userService.delete(id).pipe(first()).subscribe(() => {
        this.loadAllUsers()
      });
    }

    private loadAllUsers() {
      this.userService.getAll().pipe(first()).subscribe(users => {
        this.users = users;
      });
    }*/


    public setTabIndex(index: number) {
      this.currentIndex = index;
    }

    public setTabBenIndex(index: number) {
      this.currentIndexBenef = index;
    }
    setStep(index: number) {
      this.step = index;
    }
    setSubStep(index: number) {
      this.subStep = index;
    }
    
    nextStep() {
      this.step++;
    }

    prevStep() {
      this.step--;
    }

    moveToSelectedTab(tabName: string) {
      if (this.verificationFormAdmin.controls.adminsArray.invalid) {
        return;
      }
      
      this.onSubmitAdmin(this.countertabs - 1);
      this.setStep(2);

      /*for (let i =0; i< document.querySelectorAll('.mat-tab-label-content').length; i++) {
        if ((<HTMLElement>document.querySelectorAll('.mat-tab-label-content')[i]).innerText === tabName)
        {
          (<HTMLElement>document.querySelectorAll('.mat-tab-label')[i]).click();
        }
      }*/
      
    }

    addTabAdmin(selectAfterAdding: boolean) {

    //  this.countertabs = this.countertabs + 1 ;
       // if (selectAfterAdding)
       // {
       if (selectAfterAdding){
         this.tabs.push( 'Administrator  ' + (this.countertabs + 1 ));
       }
       
       this.onSubmitAdmin( this.currentIndex );
       //add dynamic validators:...
       this.addAdmin();
       // }
       // this.tabs.push('New');
      //
      if (selectAfterAdding) {
        this.setTabIndex(this.tabs.length - 1);
 //       this.selected.setValue(this.tabs.length - 1);
      }
      // this.onSubmitAdmin(this.currentIndex);
    }

    addTabBeneficiar(selectAfterAdding: boolean) {

    //  this.countertabsBenef = this.countertabsBenef + 1 ;
       // if (selectAfterAdding)
       // {
       if (selectAfterAdding){
         this.tabsBenef.push( 'Benefieciary ' + this.countertabsBenef + 1);
       }
       // }
       // this.tabs.push('New');
      //

      this.onSubmitBeneficiar(this.currentIndexBenef);
      this.addBeneficiar();

      if (selectAfterAdding) {
        //this.selected.setValue(this.tabsBenef.length - 1);
        this.setTabBenIndex(this.tabsBenef.length - 1);
      }

      // this.onSubmitBeneficiar(this.currentIndexBenef + 1);

    }

    removeTab(index: number) {
      this.tabs.splice(index, 1);
      this.countertabs = this.countertabs - 1 ;
      if (this.currentIndex >= this.countertabs ) {
        this.currentIndex -= 1;
      }
      this.adminsArray.splice(index, 1);
    }
    removeTabBenef(index: number) {
      this.tabsBenef.splice(index, 1);
      this.countertabsBenef = this.countertabsBenef - 1 ;

      if (this.currentIndexBenef >= this.countertabsBenef ) {
        this.currentIndexBenef -= 1;
      }
      this.bensArray.splice(index, 1);
    
    }

    //on submit first step/tab trust info -> should go to administrators, than to benefs
    onGrantorSubmit() {
      this.submitted = true;

      // stop here if form is invalid
      if (this.verificationForm.invalid) {
        return;
      }

      this.loading = true;
      let fieldsObj=this.verificationForm.value;
      delete fieldsObj['__proto__'];
      let dataObj: any = {
        userId:this.currentUser._id,
        //fields:{
          grantor: fieldsObj,
        //}
      };
      if (dataObj.grantor.address !== '' && dataObj.grantor.address.toString().match(/[\s\S]+[\r\n]+[.]*/g)) {
        dataObj.grantor.address = dataObj.grantor.address.replace(/[\r\n]/g, ' ');
      }
      this.dataService.trust = dataObj;
        console.log(this.verificationForm.value)
        this.alertService.success("Grantor was Submit!");
        /*for (let i =0; i< document.querySelectorAll('.mat-tab-label-content').length; i++) {
          if ((<HTMLElement>document.querySelectorAll('.mat-tab-label-content')[i]).innerText === tabName)
          {
            (<HTMLElement>document.querySelectorAll('.mat-tab-label')[i]).click();
          }
        }*/
        /// garntor ommited: copied fromthis.nextStep();
      // })
    }

    submittedTrust(tabName: string){
      this.submitted = true;

      // stop here if form is invalid
      if (this.verificationFormTrust.invalid) {
        return;
      }
      var dataObj: any ={
        userId:this.currentUser._id,
        // fields:{
          trustName: this.verificationFormTrust.value.FullName,
          administrators:[],
          beneficiaries:[],
          grantor: null
        //}
      }
      this.dataService.trust = dataObj;
       // console.log(response)
        this.alertService.success("Trust was entered! Full name: "+this.verificationFormTrust.value.FullName);

        //sybmit grantor also as user

     //   this.onGrantorSubmit();
        this.nextStep();
        /*for (let i =0; i< document.querySelectorAll('.mat-tab-label-content').length; i++) {
          if ((<HTMLElement>document.querySelectorAll('.mat-tab-label-content')[i]).innerText === tabName)
          {
            (<HTMLElement>document.querySelectorAll('.mat-tab-label')[i]).click();
          }
        }*/
      // })
    }
    onSubmitDoc(){
      var endpoint = environment.apiUrl
      var iddoc = this.currentUser._id
      window.open(endpoint + '/doc?userId=' + iddoc);

      const headers = {'authorization': this.authenticationService.currentUserValue.token};
      // whuy is this call here?
      this.authenticationService.httpGET('/doc', {userId: this.currentUser._id, method: "download"})
      .subscribe((response) => {
         console.log(response);
      });

    }


    public onSubmitAdmin(index: number) {
      this.submitted = true;

      // stop here if form is invalid
      if (this.adminsArray[ index ].invalid) {
        return;
      }

      this.loading = true;
console.log(this.verificationFormAdmin.value);

     //const fieldsObj=Object.keys(this.verificationFormAdmin.value.adminsArray ).map(i => this.verificationFormAdmin.value.adminsArray[i]);
     const fieldsObj = this.verificationFormAdmin.value.adminsArray;
     // const fieldsObj = this.verificationFormAdmin.controls.adminsArray.value;
     
     delete fieldsObj['__proto__'];
      var dataObj: any ={
        userId:this.currentUser._id,
        push:'',
        //fields:{
          administrators: fieldsObj,
        //}
      }
      this.dataService.trust = dataObj;
     //   console.log(response)
        this.alertService.success("Admin was Submit! Name: "+this.admins[this.currentIndex].controls.firstName.value + ' ' + this.admins[this.currentIndex].controls.lastName.value );  

      // })
      if (index + 1 < this.tabs.length) {
      this.setTabIndex(index + 1);
      } else {
     // this.tabs.splice(index, 1);
        this.setStep(this.step + 1);
      }
    }

    onSubmitBeneficiar(index: number) {
      this.submitted = true;

      // stop here if form is invalid
      if (this.bens[ index ] && this.bens[ index ].invalid) {
        return;
      }

      this.loading = true;
console.log(this.verificationFormBeneficiar.value);
      var fieldsObj=Object.keys(this.bensArray).map(i => this.bensArray[i].value);
//var fieldsObj=Object.keys(this.bensArray[ index - 1].value ).map(i => this.verificationFormBeneficiar.value.bensArray[i]);

      delete fieldsObj['__proto__'];
      var dataObj: any ={
        userId:this.currentUser._id,
        push:'',
        // fields:{
        beneficiaries: fieldsObj,
        //}
      }
      this.dataService.trust = dataObj;
        console.log(this.verificationFormBeneficiar.value);
     this.alertService.success("Benefieciary was Submit! Full Name: " + this.bens[this.currentIndexBenef].controls.fullName.value );

    //  if (index + 1 < this.tabsBenef.length) {
    //   this.setTabBenIndex(index + 1);
    //  } else {
    //    // if last, finish
    //    // this.setTabBenIndex(index + 1);
    //  }
     // })

     // this.tabsBenef.splice(index, 1);
    }

    public onFinish() {

      /* last benefactor could be added on 'Finish' button click */
      if (this.verificationFormBeneficiar.controls.bensArray.invalid) {
        return;
      }
      this.onSubmitBeneficiar(this.tabsBenef.length - 1 );
      // this.verificationForm.invalid ||
      if ( this.verificationFormTrust.invalid || this.adminsArray.find(f=>f.invalid) !== undefined
        || this.bensArray.find(f=>f.invalid) !== undefined) {
            // if(this.verificationForm.invalid){
            //   this.step = 0;
            // } else 
            if (this.verificationFormTrust.invalid) {
              this.step = 0;
              this.subStep = 2;
            } else if (this.adminsArray.find(f=>f.invalid) !== undefined) {
              this.step = 1;
            } else if (this.bensArray.find(f=>f.invalid) !== undefined) {
              this.step = 2;
            }
            return;
        }

      this.authenticationService.httpRequest("POST",'/trust', this.dataService.trust, { observe: 'response' })
      .subscribe( (response)=>{
           console.log(response);
           if (response['error'] === undefined) {
              this.alertService.success('Thank you. Your trust documents are being reviewed and prepared.' +
              '  You will receive an email when these are ready for the next step.', true);
              this.sideBarService.isOpen = false;
              this.router.navigateByUrl('/home');
           } else {
            this.alertService.error('Trust was not submitted. ' + response );
           }
         },
         (error: any) => {
           console.log(error);
          this.alertService.error('Trust was not submitted. ' + error.error.error);
         });
    }
    // handleError(data: any): any {
    //   this.alertService.error('Trust was not submitted. ' + data.error);
    //   return of(data);
    // }
  }
