﻿import { Component, HostListener, ViewChild, ElementRef, OnInit, ComponentRef } from '@angular/core';
import { Router, NavigationStart, NavigationEnd } from '@angular/router';

import { AuthenticationService } from './_services';
import { User } from './_models';
import { SideBarService } from './_services/sidebar.service';
import { MatDrawer, MatSidenav } from '@angular/material';
import { SideBarComponent } from './_components/sidebar.component';

@Component({ selector: 'app', templateUrl: './app.component.html', styleUrls: ['./app.component.css']})
export class AppComponent implements OnInit {
    @ViewChild('asideBar', { static: true})
    asideBar: SideBarComponent;

    @ViewChild('drawer', { static: true})
    drawer: MatSidenav;

    currentUser: User;
    public get isPreview(): boolean {
        return (this.asideBar.view !== undefined && this.asideBar.view.pdfPreview.url != null);
    }

    navigationSubscription: any;

    constructor(
        private router: Router,
        private authenticationService: AuthenticationService,
        public sideBarService: SideBarService
    ) {
        this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
    }

    ngOnInit(): void {
        this.navigationSubscription = this.router.events.subscribe((e: any) => {
            if (e instanceof NavigationEnd && (e.url.indexOf('/home') !== -1 || e.url === '/')) {
                this.drawer.close();
            }
        });
        this.sideBarService.change.subscribe( (s: any) => {
            if (s && s.data !== '') {
                if (s.url !== undefined && s.url !== null && s.url) {
                    this.drawer.mode = 'over';
                } else {
                    this.drawer.mode = 'side';
                }
                if (s.data !== undefined) {
                    this.drawer.open();
                }
            } else {
                this.drawer.close();
            }
        });
    }

    logout() {
        this.authenticationService.logout();
        this.router.navigate(['/login']);
    }

    // Logout user if window/tab closes
    @HostListener('window:beforeunload', ['$event'])
    beforeunloadHandler(event) {
        if (this.authenticationService.currentUserValue) {
            this.authenticationService.logout();
            this.router.navigateByUrl('/login');
        }
    }
}
