﻿import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home';
import { LoginComponent } from './login';
import { RegisterComponent } from './register';
import { AuthGuard } from './_guards';

import { StatisticsComponent } from './statistics';
import { MyPasswordComponent } from './my-password';
import { VerifyEmailComponent } from './verify-email';
import { NextstepComponent } from './nextstep';
import { FirststepComponent } from './firststep';
import { DocModuleComponent } from './doc-module';
import { UserComponent } from './user/user.component'
import { TrustInfoComponent } from './trust-info/trust-info.component';
import { TrustInfoDetailComponent } from './trust-info-detail/trust-info-detail.component';
import { UserDocModuleComponent } from './user-doc-module';







const appRoutes: Routes = [
    { path: '', component: HomeComponent /*, canActivate: [AuthGuard]*/ },
    { path: 'login', component: LoginComponent },
    { path: 'login/:reset', component: LoginComponent },
    { path: 'login/verify/:token', component: LoginComponent },
    { path: 'register', component: RegisterComponent/*,
runGuardsAndResolvers: 'paramsOrQueryParamsChange' */},
    { path: 'register/:resend', component: RegisterComponent},
    { path: 'register/:resendEmail', component: RegisterComponent},
    { path: 'statistics', component: StatisticsComponent },
    { path: 'my-password', component: MyPasswordComponent },
    { path: 'verify-email', component: VerifyEmailComponent },
    { path: 'nextstep', component: NextstepComponent /*, canActivate: [AuthGuard]*/ },
    { path: 'user', component: UserComponent/*, canActivate: [AuthGuard]*/ },
    { path: 'trust-info', component: TrustInfoComponent,/* canActivate: [AuthGuard] */},
    { path: 'trust-info/view', component: TrustInfoDetailComponent/*, canActivate: [AuthGuard]*/ },
    { path: 'user-doc/info/:userDocType', component: UserDocModuleComponent, /* canActivate: [AuthGuard]*/ },
    {path: 'firststep', component: FirststepComponent},
      {path: 'doc-module', component: DocModuleComponent /*, canActivate: [AuthGuard]*/},

    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

export const routing = RouterModule.forRoot(appRoutes, { onSameUrlNavigation: 'reload' });
