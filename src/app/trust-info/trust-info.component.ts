import { Component, OnInit, Inject } from '@angular/core';
import { User } from '@app/_models';
import { environment } from '@environments/environment';
import { AuthenticationService } from '@app/_services';
import { TrustResponse, Trust } from '@app/_models/trust';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import { SideBarService } from '@app/_services/sidebar.service';
import { MatTableDataSource, MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { RouteStateService } from '@app/_services/route-state.service';

const ELEMENT_DATA: TrustResponse[] = [
  // { empName: 'Mark, William', title: 'Developer', address: 'Address 1' } ,
  // { empName: 'Adam, Ross', title: 'Manager', address: 'Address 2' } ,
  // { empName: 'Robert, Steve', title: 'Accountant', address: 'Address 2' } 
];


@Component({
  selector: 'app-trust-info',
  templateUrl: './trust-info.component.html',
  styleUrls: ['./trust-info.component.css']
})
export class TrustInfoComponent implements OnInit {
  currentUserSubscription: any;
  currentUser: User;
  env = environment;
  // viewModel: { isDoc: boolean } = {isDoc: null};
  public docUrl: string;
  public content: any[] = [];

  displayedColumns = [ 'trustName', 'administrators',  'beneficiaries', 'toolbar'];
  data: TrustResponse[] = []; // = Object.assign(ELEMENT_DATA);

  dataSource: MatTableDataSource<TrustResponse>;
  selection = new SelectionModel<TrustResponse>(true, []);

  constructor(private authenticationService: AuthenticationService, private router: Router, private sideBarService: SideBarService,
    public dialog: MatDialog, private routeStateService: RouteStateService) {
    this.dataSource = new MatTableDataSource<TrustResponse>(this.data);
   }
  
   openTrust(data): void {
    this.routeStateService.add("Trust Info details",'/trust-info/view', data, false);
    // this.router.navigate(['trust-info/view'], data);
  }

  /* -----------------POPUP CODE---------------------*/
  // openPopup(data): void {
  //   let dialogRef = this.dialog.open(OpenPopup, {
  //        width: '600px', height: '400px',
  //        data: data //this.selection['_selected']
  //      });
  //      dialogRef.afterClosed().subscribe(result => {
  //        console.log('The dialog was closed');
  //      });
  //    }
   
  initData() {
    this.authenticationService.httpGET('/trust') //, {userId: this.currentUser._id}
    .subscribe( (response: TrustResponse[]) => {
      // console.log(this.verificationForm.value)
      this.dataSource.data = response;
      this.content = [];

      response.forEach( (s, index) => {

        this.content[this.content.push(s) - 1].isDoc = null;

        this.authenticationService.httpGET('/doc/get', {method: 'dataURL', trustId: s.trustId})
        .subscribe((doc)=>{
          this.content[index].isDoc = doc !== undefined &&  doc !== null && doc.dataURL !== '';

       // this.viewModel.isDoc = doc !== undefined &&  doc !== null && doc.dataURL !== '';
  //      endpoint+/data/doc?userId
        });


      })
      
    });
  }

  ngOnInit() {
    this.initData();
    this.authenticationService.alerts().subscribe(
      success => {},
      error => {}
    );
    
    this.router.events.pipe(filter(e => e instanceof NavigationEnd)).subscribe((e) => {
      this.initData();
      this.authenticationService.alerts().subscribe(
        success => {},
        error => {}
      );
     });
  }

  openDoc(trustId: string, event: any) {
    // window.open(this.docUrl, '_blank');
    // try {
      event.stopPropagation();
      event.preventDefault();
 
      const str =
      `Document preview`;
    this.sideBarService.show({ data: str, url: environment.apiUrl + '/doc/get?trustId=' + trustId, params: { fileName: 'trust.pdf'}  });

  }
  // openDoc(trustId: string, event: any) {
  //   event.stopPropagation();
  //   event.preventDefault();
  //   //window.open(this.docUrl, '_blank');
  //   try {
      
  //     const headers = {
  //       responseType: 'blob' }; //arraybuffer
  //     this.authenticationService.httpGET('/doc/get', {trustId: trustId}, headers)
  //       .subscribe((doc)=>{
  //         //console.log(response);
  //       //this.viewModel.isDoc = doc !== undefined &&  doc !== null && doc.dataURL !== '';
  //       this.downloadFile(doc, 'application/pdf', 'trust.pdf');
  //       //this.docUrl = this.env['apiUrl'] + '/doc?userId=' + this.currentUser._id;
        
  //       // window.open(this.env['apiUrl'] + '/data/doc?userId=' + this.currentUser._id, '_blank');
        
  //         // this.alertService.success("Grantor was Submit!");
  //       // this.isDoc = response;
  //     },
  //     e => console.log(e));
  //   } catch(ex) {
  //     //this.viewModel.isDoc = false; 
  //   }
  // }

  downloadFile(blob: any, type: string, filename: string): string {
    const url = window.URL.createObjectURL(blob); // <-- work with blob directly
  
    // create hidden dom element (so it works in all browsers)
    const a = document.createElement('a');
    a.setAttribute('style', 'display:none;');
    document.body.appendChild(a);
  
    // create file, attach to hidden element and open hidden element
    a.href = url;
    a.download = filename;
    a.click();
    return url;
  }

  
  showHint(strOrPlug, ev: any) {

    let str = '';
    switch (strOrPlug) {
      case 'TRUSTINFO': 
        str = 'Please review your trusts info, and here you can: <br/>1) <b>download document</b> using "View docs..." button on specific Trust after <br/>it was filled and submitted and<br/>2) <b>upload the document</b> after all fields info was filled in.';
      break;
      default: str = strOrPlug; break;
    }

    this.sideBarService.show({ data: str });
    if(ev!==undefined && ev.stopPropagation !== undefined) {
     ev.stopPropagation();
    }
  }


}


/*-------------------------SELECT DEPTS RESPONSIBLE FOR ROOM POPUP COMPONENT----------------------------------*/
@Component({
  selector: 'app-popup',
  templateUrl: './popup.html'
})



export class OpenPopup {




 constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<OpenPopup> ) {

   }


saveToMainTable(){}
  
}