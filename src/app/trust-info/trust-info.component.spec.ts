import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrustInfoComponent } from './trust-info.component';

describe('TrustInfoComponent', () => {
  let component: TrustInfoComponent;
  let fixture: ComponentFixture<TrustInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrustInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrustInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
