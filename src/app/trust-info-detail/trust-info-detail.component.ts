import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import { AuthenticationService } from '@app/_services';
import { Trust, TrustResponse } from '@app/_models/trust';
import { RouteStateService } from '@app/_services/route-state.service';
import { SideBarService } from '@app/_services/sidebar.service';
import { environment } from '@environments/environment';

@Component({
  selector: 'app-trust-info-detail',
  templateUrl: './trust-info-detail.component.html',
  styleUrls: ['./trust-info-detail.component.css']
})
export class TrustInfoDetailComponent implements OnInit {
  public contents: TrustResponse;
  isDoc: boolean;
  
  private initRoutes() {
    let routeState = this.routeStateService.getCurrent();
    this.contents = routeState.data;
    this.router.events.subscribe(e => {
        if (e instanceof NavigationEnd) {
          this.contents = routeState.data;
          if (this.contents === undefined) {
            this.contents = <TrustResponse> this.router.getCurrentNavigation().extras;
          }
        }
    });
    // this.department = this.departmentService.getDepartmentById(routeState.data);
 
    // this.title = routeState.title;
  //  this.contents.trustId = routeState.data.trustId;

  }
  constructor(private router: Router, private authenticationService: AuthenticationService, private routeStateService: RouteStateService,
    private sideBarService: SideBarService ) {
    this.router.events.pipe(filter(e => e instanceof NavigationEnd)).subscribe((e: NavigationEnd) => {
      // this.initData();
     if (e.url.indexOf('/trust-info/view') === -1) {
      sideBarService.hide();
     }
      this.contents = <TrustResponse> this.router.getCurrentNavigation().extras;
      if (this.contents === undefined || ! (this.contents instanceof TrustResponse) ) {
        this.initRoutes();
      }
      console.log("4444");
      // isDoc = this.contents.

      
      this.isDoc = null;

      this.authenticationService.httpGET('/doc/get', {method: 'dataURL', trustId: this.contents.trustId})
      .subscribe((doc)=>{
        this.isDoc = doc !== undefined &&  doc !== null && doc.dataURL !== '';
      });


      console.log(this.router.getCurrentNavigation().extras);
      // this.contents = e.data;
     });
   }

   downloadDoc(trustId: string) {
    //window.open(this.docUrl, '_blank');
    try {
      const headers = {
        responseType: 'blob' }; //arraybuffer
      this.authenticationService.httpGET('/doc/get', {trustId: trustId}, headers)
        .subscribe((doc)=>{
          //console.log(response);
        //this.viewModel.isDoc = doc !== undefined &&  doc !== null && doc.dataURL !== '';
        this.downloadFile(doc, 'application/pdf', 'trust.pdf');
        //this.docUrl = this.env['apiUrl'] + '/doc?userId=' + this.currentUser._id;
        
        // window.open(this.env['apiUrl'] + '/data/doc?userId=' + this.currentUser._id, '_blank');
        
          // this.alertService.success("Grantor was Submit!");
        // this.isDoc = response;
      },
      e => console.log(e));
    } catch(ex) {
      //this.viewModel.isDoc = false; 
    }
  }


  openDoc(trustId: string) {
    // window.open(this.docUrl, '_blank');
    // try {
      const str =
      `Document preview`;

  this.sideBarService.show({ data: str, params: { fileName: 'trust.pdf'}, url: environment.apiUrl + '/doc/get?trustId=' + trustId });

  }

  downloadFile(blob: any, type: string, filename: string): string {
    const url = window.URL.createObjectURL(blob); // <-- work with blob directly
  
    // create hidden dom element (so it works in all browsers)
    const a = document.createElement('a');
    a.setAttribute('style', 'display:none;');
    document.body.appendChild(a);
  
    // create file, attach to hidden element and open hidden element
    a.href = url;
    a.download = filename;
    a.click();
    return url;
  }

  showHint(strOrPlug: string, ev: any) {

    let str = '';
    switch (strOrPlug) {
      case 'TRUSTINFO':
        str = 'Please review your trusts info, and here you can: <br/>' +
        '1) <b>download document</b> using "View docs..." button on specific Trust after <br/>'
        +'it was filled and submitted and<br/>2) <b>upload the document</b> after all fields info was filled in.';
      break;
      default: str = strOrPlug; break;
    }
    this.sideBarService.hide();
    this.sideBarService.show({data: str});
    if (ev !== undefined && ev.stopPropagation !== undefined) {
    // ev.stopPropagation();
    }
  }

  ngOnInit() {
    //console.log();
    // this.sideBarService.hide();
    
  }

}
