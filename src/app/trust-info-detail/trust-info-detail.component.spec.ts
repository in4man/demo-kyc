import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrustInfoDetailComponent } from './trust-info-detail.component';

describe('TrustInfoDetailComponent', () => {
  let component: TrustInfoDetailComponent;
  let fixture: ComponentFixture<TrustInfoDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrustInfoDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrustInfoDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
