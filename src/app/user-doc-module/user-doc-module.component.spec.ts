import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserDocModuleComponent } from './user-doc-module.component';

describe('DocModuleComponent', () => {
  let component: UserDocModuleComponent;
  let fixture: ComponentFixture<UserDocModuleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserDocModuleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserDocModuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
