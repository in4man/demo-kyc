import { Component, OnInit, Input } from '@angular/core';

import {MatDialog, MatSnackBar, MatSnackBarConfig} from '@angular/material';

import { AlertService, UserService, AuthenticationService } from '@app/_services';
import { Router, NavigationStart, ActivatedRoute, NavigationEnd } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { environment } from '@environments/environment';
import { TrustStatuses } from '@app/_models/trustStatuses';

import { SideBarService } from '@app/_services/sidebar.service';


@Component({
  selector: 'app-user-doc-module',
  templateUrl: './user-doc-module.component.html',
  styleUrls: ['./user-doc-module.component.scss'],

})
export class UserDocModuleComponent implements OnInit {
  currentUserSubscription: any;
  currentUser: any;
  @Input() trustId: string;
  // model: { currentStatus: TrustStatuses } = { currentStatus: TrustStatuses.TrustEmpty};
  model: any = {};
  public trustStatuses: any = TrustStatuses;
  messages: string;
  // tslint:disable-next-line:no-input-rename
  @Input() public userDocType: string;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authenticationService: AuthenticationService,
    private userService: UserService,
    public dialog: MatDialog,
    private route: ActivatedRoute,
    private alertService: AlertService, private toasts: MatSnackBar, private sideBarService: SideBarService
  ) {
    this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
      this.currentUser = user;
    });
    this.router.events.subscribe(s => {
      if ( s instanceof NavigationStart && s.url.indexOf('/user-doc/info') === -1) {
        this.toasts.dismiss();
        this.sideBarService.hide();
      }
    });

    this.route.params.subscribe( param => {
      if ( param['userDocType'] !== undefined ) {
        this.userDocType = param['userDocType']  as string;

        //view document: status or fileName...
        this.userService.getProduct(this.userDocType).subscribe( prod => {

          /*"original": "SPCOriginal1574339854989.pdf",
          "filled": null,
          "producStatus": "accepted",
          "filingNumber": "190000045261",
          "envelopeId": "100000016"*/

          this.model = prod;
        },
        error => {
          if (error.error !== undefined) {
            this.toasts.open('Document of type \'' + this.userDocType +
             '\' not created yet. Please download file first, and approve it by signing document and uploading document back');
          }
        })
      }
    });
  }

  public handleFileDownload(evt) {
    this.openDoc(this.model.filled !=='' ? this.model.filled : this.model.original);
  }
  public handleFileSelect(evt) {
    if (this.userDocType === '' || this.userDocType === undefined || this.userDocType === null) {
      return;
    }

    var	endpoint = environment.apiUrl
    var file = evt.target.files[0];


    /*const headers = {
      responseType: 'arraybuffer' };*/

    const formData = new FormData();
      formData.append('file', file);
      formData.append('sku', this.userDocType);

    this.authenticationService.httpRequest('PUT','/product',formData)
    .subscribe( (response) => {
      // window.scroll(0, 0);
      this.model.filled = true;
      this.alertService.success('Doc ' + this.userDocType + ' submitted.');
      this.toasts.open('Document was submitted.');
        console.log(response);
    }, (error) => {
      this.model.filled = false;
      this.alertService.error('Error. Doc ' + this.userDocType + ' was not submitted.');
    });

  /*
  openFile(file,(dataURL)=>{
      var requestObj={
          userId:this.authenticationService.currentUserValue._id,
          dataURL: dataURL

      };

    });*/

  }

  ngOnInit() {
    // this.messages = 'Document of type \'' + this.userDocType +
    // '\' has been sent. Please wait until it will be processed.';
    this.messages = 'Your ' + this.userDocType + ' documents are being filed. ';

 
    this.userService.getProduct(this.userDocType).subscribe( prod => {

      /*"original": "SPCOriginal1574339854989.pdf",
      "filled": null,
      "producStatus": "accepted",
      "filingNumber": "190000045261",
      "envelopeId": "100000016"*/

      this.model = prod;
    },
    error => {
      if (error.error !== undefined) {
        // const snackConf: MatSnackBarConfig = {
        //   panelClass: 'style-success',
        // };
        this.model.original = null;
      } else {
        this.model.original = '';
      }
    });
  }


  downloadDoc(trustId: string) {
    //window.open(this.docUrl, '_blank');
    try {
      const headers = {
        responseType: 'blob' }; //arraybuffer
      this.authenticationService.httpGET('/product/get', {sku: 'spc', method: 'download'}, headers)
        .subscribe((doc)=>{
          //console.log(response);
        //this.viewModel.isDoc = doc !== undefined &&  doc !== null && doc.dataURL !== '';
        this.downloadFile(doc, 'application/pdf', 'trust.pdf');
        //this.docUrl = this.env['apiUrl'] + '/doc?userId=' + this.currentUser._id;
        
        // window.open(this.env['apiUrl'] + '/data/doc?userId=' + this.currentUser._id, '_blank');
        
          // this.alertService.success("Grantor was Submit!");
        // this.isDoc = response;
      },
      e => console.log(e));
    } catch(ex) {
      //this.viewModel.isDoc = false; 
    }
  }


  openDoc(fileName: string) {
    // window.open(this.docUrl, '_blank');
    // try {
      const str =
      `Document '${this.userDocType}' preview`;
    this.sideBarService.show({ data: str, params: { fileName: fileName},
                                        url: environment.apiUrl + '/product/get?sku=spc&method=download' });
  }

  downloadFile(blob: any, type: string, filename: string): string {
    const url = window.URL.createObjectURL(blob); // <-- work with blob directly
  
    // create hidden dom element (so it works in all browsers)
    const a = document.createElement('a');
    a.setAttribute('style', 'display:none;');
    document.body.appendChild(a);
  
    // create file, attach to hidden element and open hidden element
    a.href = url;
    a.download = filename;
    a.click();
    return url;
  }

}
