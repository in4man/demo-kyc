﻿import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first, map } from 'rxjs/operators';
// declare var foo: userObj;

import { AlertService, AuthenticationService, UserService } from '@app/_services';
import { throwError, of, BehaviorSubject, timer } from 'rxjs';
import { User } from '@app/_models';
import { environment } from '@environments/environment';
import { Location } from '@angular/common';


@Component({templateUrl: 'login.component.html'})
export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;
    userObj:any[]=[];
    token = '';
    viewModel: any = {showReset: false};
    regtok = '';
    showLoginForm: boolean;
    showLoader: boolean;
    // convenience getter for easy access to form fields
    public get f(): any { return this.loginForm.controls; }

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticationService,
        private alertService: AlertService,
        private userService: UserService,
        private location: Location

    ) {
        // redirect to home if already logged in
        if (this.authenticationService.currentUserValue) {
            this.router.navigate(['/']);
        }
        this.showLoginForm = true;
        this.showLoader = false;
        this.route.queryParams.subscribe(params => {
  
            const autologout = params['autologout'];
            if (autologout === 'true') {
                this.showLoginForm = false;
                this.showLoader = true;
                this.authenticationService.goToExternal();
            } else {
                this.showLoginForm = true;
                this.showLoader = false;
            }

            const token = params['token'];

            if (token === undefined) {
                return;
            }

            this.userService.loginByToken(environment.acUrlRenew, token).subscribe( s => {
                         if ( s.Error !== '' &&  s.Error.toString().toLowerCase() !== 'ok') {
                            // error or token expired
                            this.showLoader = false;
                            setTimeout(function() { 
                                // error or token expired
                                this.authenticationService.goToExternal();
                            }, 2500);
                        } else {
                            this.router.navigateByUrl('/');
                        }
                        }, e => {
                        this.authenticationService.goToExternal();
                        this.showLoginForm = false;
                        this.showLoader = true;
                        // token expired
                    });
                // });
                // this.router.navigateByUrl('/');
            },
            e => {
            // this.alertService.error('Please try again, authentication failed');
                this.showLoader = false;
            });

        
        this.route.paramMap
        .subscribe( (params: any) => {
             this.token = params.params['reset'];

             if (this.token && this.token !== '') {
                this.viewModel.showReset = true;
            }

            this.regtok = params.params['token'];
            
            if (this.regtok && this.regtok !== '') {
                this.regtoken(); // register confirm
            }
        });
    }

    ngOnInit() {
        this.alertService.clear();
        this.loginForm = this.formBuilder.group({
            email: ['', [ Validators.email ]], // should check manually Validators.required
            password: ['', Validators.required],
            passwordConfirm: ['', [] /*. Validators.equals()*/ ]
        });

        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    }


    private regtoken() {
      this.authenticationService.httpGET('/user/verify/' + this.regtok)
      .subscribe( (response) =>{
        console.log(response);
      
       // this.submitted = false;

        if (response && response.result !== '') {
              this.alertService.info('You have been succesfully registered in system.');
              this.showLoader = false;
              } else { 
                  this.alertService.error('Some error occured. Please try again later');
                  this.showLoader = false;
              }
          }, error => {
                  this.alertService.error('Registration problem. ' + error.error.error);
                  this.showLoader = false;
          }
      );

    }

    public reset() {
        // stop here if form is invalid
        if (this.f.password.invalid || this.token === '') {
          return;
      }
      this.loading = true;

      this.authenticationService.httpRequest('POST', '/user/reset_password', {password: this.f.password.value, token: this.token})
      .subscribe( (response) =>{
        console.log(response);
      
       // this.submitted = false;

        if (response && response.result !== '') {
              this.alertService.infoWithLink('Password was reset successfully. ' + response.result);
              this.loading = false;
              } else { 
                  this.alertService.error('Some error occured. ');
                  this.loading = false;
              }
          }, error => {
                  this.alertService.error('Password was not set successfully. ' + error.error.error);
                  this.loading = false;
          }
      );

    }
    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.loginForm.invalid) {
            return;
        }
        

        this.loading = true;
        var dataObj={email:this.loginForm.value.email,password:this.loginForm.value.password}
        console.log(this.loginForm.value)


      // httpGET("/fields/user",dataObj,(response)=>{
      //
      //  this.userObj = {username:this.response.value.username};
      //  console.log( this.userObj);
      //  this.authenticationService.login(this.res)
      //
      // })

        // stop here if form is invalid
        if (this.loginForm.invalid) {
            return;
        }


        this.authenticationService.login(this.f.email.value, this.f.password.value)
        // console.log(this.f.username.value, this.f.password.value)

     //       .pipe(first())
       // .pipe( )
        .subscribe(
            userObjResp =>{ //success
                let retur: any;
                const userObjResponse: any = userObjResp.body;
                if (userObjResponse.email !== '' && userObjResponse.email !== undefined /*email */) { //if (userObjResponse.email=== 'verified' /*email */) {
                userObjResponse.token = userObjResp.headers.get( 'authorization' );
                retur = userObjResponse;
                } else {
                this.alertService.error('Incorrect');
                // setTimeout(function(){
                //  window.location.reload();
                // }, 3000);
                retur = null;
                }
                if (retur === null) {
                    return throwError('Incorrect login')
                } else { 
                    localStorage.setItem('currentUser', JSON.stringify(retur));
                    // this.authenticationService.currentUserSubject = new BehaviorSubject<User>(retur);
                    // this.authenticationService.currentUser = this.authenticationService.currentUserSubject.asObservable();
                    // this.authenticationService.currentUserSubject.next(this.authenticationService.currentUserValue);

                    this.authenticationService.currentUserSubject.next(
                        { ...retur, email: this.loginForm.value.email});

                    if (this.returnUrl) {
                        this.router.navigateByUrl(this.returnUrl );
                    } else {
                        this.router.navigate(['/home']);
                    }
                    return of(retur);
                }
            }, 
            user => { // error
        // login successful if there's a jwt token in the response
        // if (user && user.token) {
            // store user details and jwt token in local storage to keep user logged in between page refreshes
            this.alertService.error("Incorrect. "+ user.error.error);
            localStorage.setItem('currentUser', null);
            this.authenticationService.currentUserSubject.next(user);
            this.loading = false;
        // }
        return user;
    });
    }
}
