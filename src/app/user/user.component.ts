import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Subscription } from 'rxjs';
import { first, filter } from 'rxjs/operators';
import {MatTabsModule} from '@angular/material/tabs';
import {MatStepperModule} from '@angular/material/stepper';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatDialogModule} from '@angular/material/dialog';
import {MatDialog, MatTableDataSource} from '@angular/material';
import { MatCheckboxModule } from '@angular/material/checkbox';

// import { Countries } from '@app/_models/countries.ts'
import {MatDatepickerModule,MatNativeDateModule,MatIconModule} from '@angular/material';
import {
  MatButtonModule,
  MatFormFieldModule,
  MatInputModule,
  MatRippleModule } from '@angular/material';
  import { User } from '@app/_models';
  import { AlertService, UserService, AuthenticationService } from '@app/_services';
  import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
  import { FormBuilder, FormGroup, Validators, FormControl, ValidationErrors } from '@angular/forms';
import { TrustResponse } from '@app/_models/trust';
import { environment } from '../../environments/environment';
import { errorMessages } from '@app/nextstep';
import { SideBarService } from '@app/_services/sidebar.service';
import { HttpClient } from '@angular/common/http';
import { SelectionModel } from '@angular/cdk/collections';


// export interface Element {
//   trustName: string;
//   title: string;
//   address: string;
// }

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'], preserveWhitespaces: false
})
export class UserComponent implements OnInit {
  currentUserSubscription: any;
  currentUser: User;
  env = environment;
  objectKeys = Object.keys;
  // viewModel: { isDoc: boolean } = {isDoc: null};
  public docUrl: string;
  public countries: any[]; // Countries.sort();
  public states: any[];
  public cities: any[];
  ssnMask = [/\d/, /\d/, /\d/, '\-', /\d/, /\d/, '\-', /\d/, /\d/, /\d/, /\d/];
  _showProfile: boolean = false;
  isAddressInvalid: boolean;
  isExtendedAddressInvalid: boolean;
  @Input('showProfile')
  set showProfile( val: boolean) {
    this._showProfile = val;
  }

  public isSpcCompleted: boolean = false;

  get showProfile(): boolean {
    if( this.route.snapshot.queryParams['showProfile'] !== undefined) {
      return true;
     } else { 
       return this._showProfile;
      }
  }
  // model
  verificationForm: FormGroup;
  verificationFormUserInfo: FormGroup;

  public content: any[] = [];
  subStep: number = 0;
  isStateDropdown: boolean = false;

  

  private isNotNull(val: any) {
    return(val !== undefined && val !== null);
  }

  // Function to compare two objects by comparing their `unwrappedName` property.
   compareFn = (a, b) => {
    if (a.name < b.name) {
      return -1;
    }
    if (a.name > b.name) {
      return 1;
    }
    return 0;
  }

  constructor(private http: HttpClient, private authenticationService: AuthenticationService, private formBuilder: FormBuilder, private userService: UserService, 
    private alertService: AlertService, private route: ActivatedRoute, private router: Router, private sideBarService: SideBarService) {
    this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
      this.currentUser = user;
    });
    this.currentUser = this.authenticationService.currentUserValue;

    this.userService.getProduct('spc').subscribe( prod => {
      this.isSpcCompleted =  prod['filled'] !== '' && prod['filled'] !== null && prod['producStatus'] === 'accepted' ;
    });

    http.get('assets/countries.json').subscribe( (all: any[]) => {
      this.countries = all.sort(this.compareFn);

      this.userService.getAddress().subscribe(userAddress => {
      // const firstName = this.authenticationService.currentUserValue.firstName;
      // const lastName = this.authenticationService.currentUserValue.lastName;
      // const email = this.authenticationService.currentUserValue.email;
      // authenticationService.currentUserSubject.next({ ...userAddress, firstName, lastName, email });
      // user information
      this.verificationForm.controls.firstName.setValue( userAddress.firstName);
      this.verificationForm.controls.lastName.setValue( userAddress.lastName);
      this.verificationForm.controls.number.setValue( userAddress.number);
      
      // this.verificationForm.controls.email.setValue( userAddress.email);
     // const CountriesLocal = this.countries.sort();
      this.verificationForm.controls.country.valueChanges.subscribe(value => {
        console.log('country has changed:', value);
        if(value === "United States of America") {
          this.states = Object.keys(this.countries.sort().filter(f => f.name === value)[0].states);
          this.isStateDropdown = true;
        } else {
          this.isStateDropdown = false;
        }
      });
      
      // this.verificationForm.controls.state.valueChanges.subscribe(value => {
      //   console.log('state has changed:', value);
      //   if(this.isStateDropdown) {
      //     this.cities = this.countries.sort().filter(f => f.name ===  this.verificationForm.controls.country.value)[0].states[value];
      //   }
      // });

      
      this.verificationForm.controls.country.setValue( userAddress.country);
      //
       //if USA:
       if (this.verificationForm.value.country === 'United States of America') {
        this.userService.getExtendedUser().subscribe( data => {
          // user/profile_extended

          /*"gender": "man",
              "birthName": "King",
              "middleName": "Middle",
              "birthCertificateName": "birth234908",
              "birthState": "minn",
              "birthCertificateNUmber": "numb43534786",
              "dateOfBirth": "1980-11-19 09:37:52",
              "socialSecurityNumber": "507-58-3452",
              "socialSecurityBondNumber": "34534",
              "driversLicenseNumber": "F25592150094",
              "passportNumber": "SC345345PAS",
              "CertifiedMailNumber": "n*/
          
                  this.verificationFormUserInfo.controls.gender.setValue( this.isNotNull(data.gender) ?  data.gender : '');
                  // address information
                  this.verificationFormUserInfo.controls.birthName.setValue( this.isNotNull(data.birthName) ?  data.birthName : ''); 
                  this.verificationFormUserInfo.controls.middleName.setValue( this.isNotNull(data.middleName ) ? data.middleName : '');
                  this.verificationFormUserInfo.controls.birthCertificateName.setValue(
                    this.isNotNull(data.birthCertificateName) ? data.birthCertificateName : '' );
                  this.verificationFormUserInfo.controls.birthState.setValue(this.isNotNull(data.birthState ) ? data.birthState : '' );
                  this.verificationFormUserInfo.controls.birthCertificateNUmber.setValue(this.isNotNull(data.birthCertificateNUmber ) ? 
                    data.birthCertificateNUmber : '' );
                  this.verificationFormUserInfo.controls.dateOfBirth.setValue(this.isNotNull(data.dateOfBirth ) ?
                  new Date(Date.parse(data.dateOfBirth)).toISOString() : '' );
                  this.verificationFormUserInfo.controls.socialSecurityNumber.setValue(
                    this.isNotNull(data.socialSecurityNumber ) ? data.socialSecurityNumber : '' );
                  this.verificationFormUserInfo.controls.socialSecurityBondNumber.setValue(
                    this.isNotNull(data.socialSecurityBondNumber ) ? data.socialSecurityBondNumber : '' );
                  this.verificationFormUserInfo.controls.driversLicenseNumber.setValue(
                    this.isNotNull(data.driversLicenseNumber ) ? data.driversLicenseNumber : '' );
                  this.verificationFormUserInfo.controls.CertifiedMailNumber.setValue(
                    this.isNotNull(data.CertifiedMailNumber ) ? data.CertifiedMailNumber : '' );
                  this.verificationFormUserInfo.controls.passportNumber.setValue(
                      this.isNotNull(data.passportNumber ) ? data.passportNumber : '' );

                  this.verificationFormUserInfo.controls.county.setValue(
                    this.isNotNull(data.county ) ? data.county : '' );

                  this.verificationFormUserInfo.controls.MarriageState.setValue(
                    this.isNotNull(data.MarriageState ) ? data.MarriageState : '' );

                  this.verificationFormUserInfo.controls.marriageLicNum.setValue(
                  this.isNotNull(data.marriageLicNum ) ? data.marriageLicNum : '' );

                  this.verificationFormUserInfo.controls.SpouseStrawman.setValue(
                    this.isNotNull(data.SpouseStrawman ) ? data.SpouseStrawman : '' );

                  this.verificationFormUserInfo.controls.MarriageDate.setValue(this.isNotNull(data.MarriageDate ) ?
                    new Date(Date.parse(data.MarriageDate)).toISOString() : '' );

                });

      }
      // address information
      this.verificationForm.controls.address.setValue( this.isNotNull(userAddress.address) ?  userAddress.address : ''); 
      this.verificationForm.controls.city.setValue( this.isNotNull(userAddress.city ) ? userAddress.city : '');
      this.verificationForm.controls.state.setValue( this.isNotNull(userAddress.state) ? userAddress.state : '' );
      this.verificationForm.controls.zip.setValue(this.isNotNull(userAddress.zip ) ? userAddress.zip : '' );
  
    });
    });

    this.verificationForm = this.formBuilder.group({
      firstName: ['', {Validators: [ Validators.required ], disabled: true}],
      lastName: ['', {Validators: [ Validators.required ], disabled: true}],
      email: ['', {Validators: [ ], disabled: true}/* [Validators.required, Validators.email]*/],
      address: ['', Validators.required],
      country: ['', Validators.required],
      city: ['', Validators.required],
      state: ['', Validators.required],
      zip: ['', Validators.required],
      number: ['', Validators.required]
    });

    
    this.verificationFormUserInfo = this.formBuilder.group({
      'gender': ['', Validators.required],
      'birthName': ['', Validators.required],
      'middleName': ['', Validators.required],
      'birthCertificateName': ['', Validators.required],
      'birthState': ['', Validators.required],
      'birthCertificateNUmber': ['', Validators.required],
      'dateOfBirth':  ['', Validators.required],
      'socialSecurityNumber':  ['', [Validators.required,
        Validators.pattern(/^(?!0{3}|6{3}|9[0-9]{2})[0-9]{3}-?(?!0{2})[0-9]{2}-?(?!0{4})[0-9]{4}$/)]],
      'socialSecurityBondNumber':  ['', Validators.required],
      'driversLicenseNumber':  ['', []],
      'passportNumber':  ['', []],
      'CertifiedMailNumber':  ['', Validators.required],
      'county':  ['', Validators.required],
      'MarriageState':  ['', []],
      'marriageLicNum':  ['', []],
      'SpouseStrawman':  ['', []],
      'MarriageDate':  ['', []],
    });

   }

   setSubStep(index: number) {
    this.subStep = index;
  }
  
// getFormValidationErrors
getErrors(form: FormGroup) {
  let errorsToShow: string[] = [];
  Object.keys(form.controls).forEach(key => {

  const controlErrors: ValidationErrors = form.get(key).errors;
  if (controlErrors != null) {
    let errorToShow: string;
        Object.keys(controlErrors).forEach(keyError => {
          const bErrorToShow: boolean = controlErrors[keyError];
          if(bErrorToShow) {
            errorToShow = keyError;
            if (errorToShow !== null && errorToShow !== undefined && errorToShow !== '' && errorMessages[errorToShow] !== undefined) {
              errorsToShow.push(key[0].toUpperCase() + key.substr(1) + ': ' + errorMessages[errorToShow]);
            } else if (errorToShow !== null && errorToShow !== undefined && errorToShow !== '') {
              errorsToShow.push(key[0].toUpperCase() + key.substr(1) + ': ' + errorToShow);
            }
          }
        });
      }
    });
    return errorsToShow;
  }

  ngOnInit() {
    this.initData();
    this.authenticationService.alerts().subscribe(
      success => {},
      error => {}
    );

    this.router.events.pipe(filter(e => e instanceof NavigationEnd)).subscribe((e) => {
      this.initData();
      this.authenticationService.alerts().subscribe(
        success => {},
        error => {}
      );
     });
  }
  initData() {
    const token = this.authenticationService.currentUserValue.token;
    this.authenticationService.httpGET('/user') // here we fresh user info data, to be sure it's correct...
    .subscribe( (user) => {
      this.authenticationService.currentUserSubject.next( {...user, token});
      
      this.verificationForm.controls.email.setValue( user.email );
//was trust
      });
  }

  onSaveExtended() {
    if (this.verificationFormUserInfo.invalid) {
      return;
    }

    //this.loading = true;
    let fieldsObj = { ... this.verificationFormUserInfo.value };
    delete fieldsObj['__proto__'];
    // let dataObj: any = {
    //   //userId:this.currentUser._id,
    //   //fields:{
    //     address: fieldsObj.address,
    //     state: fieldsObj.state,
    //     zip: fieldsObj.zip,
    //     number:  fieldsObj.number,
    //     city: fieldsObj.city,
    //     country: fieldsObj.country
    //   //}
    // };
    // if (fieldsObj.address !== '' && fieldsObj.address.toString().match(/[\s\S]+[\r\n]+[.]*/g)) {
    //   fieldsObj.address = fieldsObj.address.replace(/[\r\n]/g, ' ');
    // }

    //this.dataService.trust = dataObj;
    if(fieldsObj['MarriageDate'] === null) {
      fieldsObj['MarriageDate'] = '';
    }
    this.userService.updateExtendedUser(fieldsObj).subscribe( 
      (result) => {
        let strRes = '';
        if (result && result.result ) {
          strRes = result.result;
        }

        this.isExtendedAddressInvalid = false;
        localStorage.setItem('userExtended', JSON.stringify(fieldsObj));
        this.alertService.success('Profile extended info was saved! ' + strRes, true);
        this.router.navigateByUrl('/');
      },
      (error) => {
        if( error && error.error && error.error.error ) {
          this.isExtendedAddressInvalid = true;
        } else {
          this.isExtendedAddressInvalid = false;
        }
        this.alertService.error('Profile extended info was not saved! Please try again. ' + error.error.error);
      }
    );
      console.log(this.verificationFormUserInfo.value)

  }
  onSaveAddress() {
    if (this.verificationForm.invalid) {
      return;
    }

    //this.loading = true;
    let fieldsObj=this.verificationForm.value;
    delete fieldsObj['__proto__'];
    let dataObj: any = {
      //userId:this.currentUser._id,
      //fields:{
        firstName: fieldsObj.firstName,
        lastName: fieldsObj.lastName,
          
      address: fieldsObj.address,
        state: fieldsObj.state,
        zip: fieldsObj.zip,
        number:  fieldsObj.number,
        city: fieldsObj.city,
        country: fieldsObj.country
      //}
    };
    if (dataObj.address !== '' && dataObj.address.toString().match(/[\s\S]+[\r\n]+[.]*/g)) {
      dataObj.address = dataObj.address.replace(/[\r\n]/g, ' ');
    }

    //this.dataService.trust = dataObj;
    this.userService.update(dataObj).subscribe( 
      (result) => {
        this.isAddressInvalid = false;
        let extInfoHint = '';
        if (this.isStateDropdown) {
          extInfoHint = 'Please continue and fill in extended address profile information.';
        }

        localStorage.setItem('address', JSON.stringify(dataObj));
        this.alertService.success('Profile is saved! ' + extInfoHint , true);
        this.subStep = 2;
        // this.router.navigateByUrl('/');
      },
      (error) => {
        let strErr = '';
        if (error && error.error && error.error.error) {
          this.isAddressInvalid = true;
          strErr = error.error.error;
        } else {
          this.isAddressInvalid = false;
        }
        this.alertService.error('Your address does not appear to be real, ' + strErr);
      }
    );
      console.log(this.verificationForm.value)

  }

  showHint(strOrPlug, ev: any) {

    let str = '';
    switch (strOrPlug) {
      case 'USER': 
        str = 'Fill in your <b>name and address</b>';
      break;
      default: str = strOrPlug; break;
    }

    this.sideBarService.show({data: str});
    if(ev!==undefined && ev.stopPropagation !== undefined) {
     ev.stopPropagation();
    }
  }

  onSubmit() {
    //this.submitted = true;

    // stop here if form is invalid
    if (this.verificationForm.invalid) {
      return;
    }

    //this.loading = true;
    let fieldsObj=this.verificationForm.value;
    delete fieldsObj['__proto__'];
    let dataObj: any = {
      //userId:this.currentUser._id,
      //fields:{
        address: fieldsObj.address,
        state: fieldsObj.state,
        zip: fieldsObj.zip,
        number:  fieldsObj.number,
        city: fieldsObj.city
      //}
    };
    if (dataObj.address !== '' && dataObj.address.toString().match(/[\s\S]+[\r\n]+[.]*/g)) {
      dataObj.address = dataObj.address.replace(/[\r\n]/g, ' ');
    }
    
    //this.dataService.trust = dataObj;
    this.userService.update(dataObj).subscribe(
      (result) => {
        this.alertService.success('Profile was saved. ' + result);
      },
      (error) => {
        this.alertService.error("Profile was not saved. " + error);
      }
    );
      console.log(this.verificationForm.value)
     
      /*for (let i =0; i< document.querySelectorAll('.mat-tab-label-content').length; i++) {
        if ((<HTMLElement>document.querySelectorAll('.mat-tab-label-content')[i]).innerText === tabName)
        {
          (<HTMLElement>document.querySelectorAll('.mat-tab-label')[i]).click();
        }
      }*/
      /// garntor ommited: copied fromthis.nextStep();
    // })
  }

}
