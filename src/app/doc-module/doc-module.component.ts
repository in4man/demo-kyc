import { Component, OnInit, Input } from '@angular/core';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';
import {MatTabsModule} from '@angular/material/tabs';
import {MatStepperModule} from '@angular/material/stepper';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatDialogModule} from '@angular/material/dialog';
import {MatDialog, MatSnackBar} from '@angular/material';

import {MatDatepickerModule,MatNativeDateModule,MatIconModule} from '@angular/material';
import {
  MatButtonModule,
  MatFormFieldModule,
  MatInputModule,
  MatRippleModule
} from '@angular/material';
import { User } from '@app/_models';
import { AlertService, UserService, AuthenticationService } from '@app/_services';
import { Router, NavigationStart, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {MatTooltipModule} from '@angular/material/tooltip';
import { environment } from '@environments/environment';
import { TrustStatuses } from '@app/_models/trustStatuses';
import { Navigation } from 'selenium-webdriver';

function httpRequest(method,path,dataObj,callback){
  var endpoint = environment.apiUrl

  var httpPost = new XMLHttpRequest();

  httpPost.onload = function(err) {
    if (httpPost.readyState == 4 && httpPost.status == 200){
      var response=JSON.parse(httpPost.responseText)//here you will get uploaded image id
      callback(response);
    } else {
      console.log(err);
    }


  }
  httpPost.open(method, endpoint+path, true);
  httpPost.setRequestHeader('Content-Type', 'application/json');//Specifies type of request
  httpPost.send(JSON.stringify(dataObj))
}

function openFile (file,callback) {
	  var endpoint = environment.apiUrl
    var reader = new FileReader();
    reader.onload = function(){
      var text = reader.result;//stores data from file in datURL format
      callback(text);
    };
    reader.readAsDataURL(file);
};
function httpGET(path,dataObj,callback){
  var	endpoint = environment.apiUrl
    var httpGet = new XMLHttpRequest();
    httpGet.onreadystatechange = ()=>{
      if (httpGet.readyState == 4 && httpGet.status == 200) {
          var response = JSON.parse(httpGet.responseText);
          callback(response)
      }
    };
    var queryString = Object.keys(dataObj).map(function(key) {
        return key + '=' + dataObj[key]
    }).join('&');
    httpGet.open('GET', endpoint+path+"?"+queryString, true);
    httpGet.send();
}





@Component({
  selector: 'app-doc-module',
  templateUrl: './doc-module.component.html',
  styleUrls: ['./doc-module.component.scss'],

})
export class DocModuleComponent implements OnInit {
  currentUserSubscription: any;
  currentUser: any;
  @Input() trustId: string;
  model: { currentStatus: TrustStatuses, filled: boolean} = { currentStatus: TrustStatuses.TrustEmpty, filled: false };
  public trustStatuses: any = TrustStatuses;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authenticationService: AuthenticationService,
    private userService: UserService,
    public dialog: MatDialog,
    private route: ActivatedRoute,
    private alertService: AlertService, private toasts: MatSnackBar
  ) {
    this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
      this.currentUser = user;
    });
    this.router.events.subscribe(s=> {
      if( s instanceof NavigationStart && s.url !== '/trust-info/view') {
        this.toasts.dismiss();
      }
    });
  }

  public handleFileSelect(evt) {
    if (this.trustId === '' || this.trustId === undefined || this.trustId === null) {
      return;
    }

    var	endpoint = environment.apiUrl
    var file = evt.target.files[0];


    /*const headers = {
      responseType: 'arraybuffer' };*/

    const formData = new FormData();
      formData.append('file', file);
      formData.append('trustId', this.trustId);

    this.authenticationService.httpRequest('PUT','/doc',formData)
    .subscribe( (response)=>{
      this.model.filled = true;
      // window.scroll(0, 0);
      this.alertService.success('Doc submitted.');
      this.toasts.open('Document was submitted.');
        console.log(response)
    });

  /*
  openFile(file,(dataURL)=>{
      var requestObj={
          userId:this.authenticationService.currentUserValue._id,
          dataURL: dataURL

      };

    });*/

  }

  ngOnInit() {
    // var name: string;
    // const token = this.authenticationService.currentUserValue.token;
    // const headers = {'authorization': this.authenticationService.currentUserValue.token};
   
  }

  download() {
    var	endpoint = environment.apiUrl;
   // httpGET('/data/doc',{userId:'5d55413393a5416114a113df',method:"download"});
   window.open(endpoint+'/doc?userId='+this.currentUser._id+'&method=download')
}
open(){
  var	endpoint = environment.apiUrl;
  window.open(endpoint+'/doc?userId='+this.currentUser._id)
}


  update(event) {
    let files = event.target.files;
    let id = this.currentUser._id;
    console.log(files);
    // dataURL = 'C:\Users\Lipa\Desktop\1'
    var requestObj={
        userId: id,
        dataURL: files
      }


    httpRequest("PUT",'/doc',requestObj,(response)=>{
        console.log(response)
    })
  }
}
