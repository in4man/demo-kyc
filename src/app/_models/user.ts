export class User {
  _id: string;
  password: string;
  firstName: string;
  number: number;
  email: string;
  lastName: string;
  token: string;
  tempToken: string;

  country: string;
  address: string;
  city: string;
  state: string;
  zip: string;
}
