export class Trust {
  _id: string;
  userId: string;
  push: string;
  // [depr] fields: TrustFields;
  // password: string;
  // firstName: string;
  // number: number;
  // email: string;
  // lastName: string;
  // token: string;

  trustStatus: string;
  trustName: string;
  //email: string;
  administrators: AdministratorFields[];
  public set adminstrators(value) {
    this.administrators = value;
  }
  public get adminstrators(): AdministratorFields[] {
    return this.administrators;
  }
  beneficiaries: BeneficiarFields[];
  grantor: GrantorFields;
  trustId: string;
}
/*
export class TrustFields {
  trustName: string;
  //email: string;
  administrators: AdministratorFields[];
  beneficiaries: BeneficiarFields[];
  grantor: GrantorFields;
  trustId: string;
}*/

export class GrantorFields {
  firstName: string;
  lastName: string;
  address: string;
  country: string;
  city: string;
  state: string;
  zip: string;
  email: string;
}
export class AdministratorFields {
  firstName: string;
  lastName: string;
  address: string;
  country: string;
  state: string;
  zip: string;
  city: string;
  email: string;

  get Address1(): string {
    return this.address;
  }
  
  set Address1(val: string) {
    this.address = val;
  }
}
export class BeneficiarFields {
  FullName: string;
  public get fullName() {
    return this.FullName;
  }
}

export class TrustResponse extends Trust /*TrustFields*/ {
}
