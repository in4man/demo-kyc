
export enum TrustStatuses {
      TrustSubmitted,
      TrustRejected,
      TrustEmpty,
      TrustFilled,
      TrustAccepted,
      DocSubmitted
    }