﻿import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { first, filter } from 'rxjs/operators';
import {MatTabsModule} from '@angular/material/tabs';
import {MatStepperModule, MatStepper, MatStep} from '@angular/material/stepper';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatDialogModule} from '@angular/material/dialog';
import {MatDialog} from '@angular/material';
import { Info1Component } from './info1/info1.component';
import { IndoForAmountComponent1 } from './indo-for-amount/indo-for-amount.component';
import {MatDatepickerModule,MatNativeDateModule,MatIconModule} from '@angular/material';
import {
  MatButtonModule,
  MatFormFieldModule,
  MatInputModule,
  MatRippleModule
} from '@angular/material';
import { User } from '@app/_models';
import { AlertService, UserService, AuthenticationService } from '@app/_services';
import { Router, NavigationEnd } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TrustStatuses } from '@app/_models/trustStatuses';
import { SideBarService } from '@app/_services/sidebar.service';
import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
declare var $: any;

@Component({ templateUrl: './home.component.html',
styleUrls: ['./home.component.scss'],
providers: [
  {
    provide: STEPPER_GLOBAL_OPTIONS,
    useValue: { showError: true }
  }
]
 })
export class HomeComponent implements OnInit, OnDestroy {
    currentUser: User;
    currentUserSubscription: Subscription;
    users: User[] = [];
    verificationForm: FormGroup;
    loading = false;
    submitted = false;
    status = false;
    public trustStatuses: any = TrustStatuses;
    model: { currentStatus: TrustStatuses } = { currentStatus: TrustStatuses.TrustEmpty};
    isSpcCompleted = false;
    @ViewChild('step', { static: true})
    step: MatStep;
    private address: any;
    extAddress: any;

    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private authenticationService: AuthenticationService,
        private userService: UserService,
        public dialog: MatDialog,
        private alertService: AlertService
    ) {
      this.isSpcCompleted = false;
      this.userService.getProduct('spc').subscribe( prod => {
          this.isSpcCompleted =  prod['filled'] !== '' && prod['filled'] !== null && prod['producStatus'] === 'accepted' ;
      });

        this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
          this.currentUser = user;

          this.authenticationService.alerts().subscribe(
            success => this.model.currentStatus = success.currentStatus,
            error => {}
          );
        });
        // this.model = null;

        this.authenticationService.alerts().subscribe(
          success => this.model.currentStatus = success.currentStatus,
          error => {}
        );

        this.address = localStorage.getItem('address');
  
        if (!this.address) {
          this.userService.getAddress().subscribe(userAddress => {
            this.address = userAddress;
          });
        }

        this.userService.getExtendedUser().subscribe(userAddress => {
          this.extAddress = userAddress;
        });

        this.router.events.pipe(filter(d => d instanceof NavigationEnd)).subscribe( (e: NavigationEnd) => {
          this.userService.getAddress().subscribe(userAddress => {
              this.address = userAddress;
          });
        });
    }

    

    public get showSPC(): boolean {

      // return (this.address !== undefined && this.address !== null && this.isNotNull(this.address) );
      // return this.model.currentStatus !== TrustStatuses.TrustFilled && (
      // this.model.currentStatus === this.trustStatuses.TrustAccepted ||
      // this.model.currentStatus === this.trustStatuses.TrustEmpty ||  this.model.currentStatus === this.trustStatuses.TrustRejected );

      return !this.isSpcCompleted && (this.address !== undefined && this.address !== null && this.isNotNull(this.address.address) ) &&
      this.address.country === 'United States of America' &&
        this.isNotNull(this.address.city) && this.isNotNull(this.address.country)
        && this.isNotNull(this.address.state) && this.isNotNull(this.address.zip) ;
    }

    public get isExtendedInfoCompleted(): boolean {

      // return (this.address !== undefined && this.address !== null && this.isNotNull(this.address) );
      // return this.model.currentStatus !== TrustStatuses.TrustFilled && (
      // this.model.currentStatus === this.trustStatuses.TrustAccepted ||
      // this.model.currentStatus === this.trustStatuses.TrustEmpty ||  this.model.currentStatus === this.trustStatuses.TrustRejected );
/*CertifiedMailNumber: "Test"
birthCertificateNUmber: "Test↵"
birthCertificateName: "Test↵"
birthName: "William Or"
birthState: "Alaska"
county: "Test"
dateOfBirth: "2019-10-22T21:00:00.000Z"
driversLicenseNumber: "Test"
gender: "man"
id: 32
middleName: "tt"
passportNumber: "Test"
socialSecurityBondNumber: "Test"
socialSecurityNumber: "222-22-2222"*/

      return (this.extAddress !== undefined && this.extAddress.CertifiedMailNumber !== null 
        && this.isNotNull(this.extAddress.birthCertificateNUmber) ) &&
        this.isNotNull(this.extAddress.birthCertificateName) && this.isNotNull(this.extAddress.birthName)
        && this.isNotNull(this.extAddress.birthState) && this.isNotNull(this.extAddress.county)
        && this.isNotNull(this.extAddress.dateOfBirth)
        && this.isNotNull(this.extAddress.driversLicenseNumber)
        && this.isNotNull(this.extAddress.gender)
        && this.isNotNull(this.extAddress.middleName)
        && this.isNotNull(this.extAddress.passportNumber)
        && this.isNotNull(this.extAddress.socialSecurityBondNumber)
        && this.isNotNull(this.extAddress.socialSecurityNumber) ;
    }


    public get showTrust(): boolean {

      // return (this.address !== undefined && this.address !== null && this.isNotNull(this.address) );
      // return this.model.currentStatus !== TrustStatuses.TrustFilled && (
      // this.model.currentStatus === this.trustStatuses.TrustAccepted ||
      // this.model.currentStatus === this.trustStatuses.TrustEmpty ||  this.model.currentStatus === this.trustStatuses.TrustRejected );

      return ((this.showSPC && this.isSpcCompleted) || !this.showSPC) && this.address !== undefined && this.address !== null && this.isNotNull(this.address.address)
        && this.isNotNull(this.address.city) && this.isNotNull(this.address.country)
        && this.isNotNull(this.address.state) && this.isNotNull(this.address.zip) ;
    }

      isNotNull(str: string) {
        return str !== null && str !== undefined && str !== '';
      }
    ngOnInit() {
        // this.loadAllUsers();
        let name: string;


        this.verificationForm = this.formBuilder.group({
            idUser: this.currentUser,
            iTIN: ['', Validators.required],
            fTIN: ['', Validators.required],
            sSN: ['', Validators.required],
            dataOfBirth: ['', Validators.required],
            country: ['', Validators.required],
            city: ['', Validators.required],
            familyMonthlyIncome: ['$', Validators.required],
            incomingFromInvesting: ['$', Validators.required],
            otherIncome: ['$', Validators.required],
            termLoan: ['$', Validators.required],
            loanAmountRequired: ['$', Validators.required],

        });

    }

    ngOnDestroy() {
        // unsubscribe to ensure no memory leaks
        this.currentUserSubscription.unsubscribe();
    }

      moveToSelectedTab(tabName: string) {
        for (let i =0; i< document.querySelectorAll('.mat-tab-label-content').length; i++) {
          if ((<HTMLElement>document.querySelectorAll('.mat-tab-label-content')[i]).innerText === tabName)
     {
        (<HTMLElement>document.querySelectorAll('.mat-tab-label')[i]).click();
     }
   }
  }
/*
    deleteUser(id: number) {
        this.userService.delete(id).pipe(first()).subscribe(() => {
            this.loadAllUsers()
        });
    }
*//*
    private loadAllUsers() {
        this.userService.getAll().pipe(first()).subscribe(users => {
            this.users = users;
        });
    }*/

    openDialog(): void {
    const dialogRef = this.dialog.open(Info1Component, {
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');

    });
  }
  onSubmit() {
      this.submitted = true;

      var name: string;
      switch (name){
        case "trustRejected": {
          this.alertService.error("Trust Rejected");
          break;
        }
        case "trustSubmitted": {
          this.alertService.error("Trust Submitted");
          break;
        }
        default: {
          this.alertService.warning("Nothing doesn't submitted");
        break;
        }
      }

      // stop here if form is invalid
      if (this.verificationForm.invalid) {
          return;
      }

      this.loading = true;
      this.userService.verify(this.verificationForm.value)
          .pipe(first())
          .subscribe(
              data => {
                  this.alertService.success('Registration successful', true);
                  this.router.navigate(['/login']);
              },
              error => {
                  this.alertService.error(error);
                  this.loading = false;
              });
  }
  openDialogAmount(): void {
  const dialogRef = this.dialog.open(IndoForAmountComponent1, {
  });

  dialogRef.afterClosed().subscribe(result => {
    console.log('The dialog was closed');

  });
}
}
