import { Injectable, Output, EventEmitter } from '@angular/core'
import { SafeHtml, SafeUrl } from '@angular/platform-browser';

@Injectable()
export class SideBarService {

  isOpen = false;

  @Output() change: EventEmitter<{ data: SafeHtml, params: any, url: SafeUrl }> = new EventEmitter();

  show(data: any) {
    this.change.emit(data);
    this.isOpen = true; // !this.isOpen;
  }

  hide() {
      this.change.emit(null);
      this.isOpen = false; // !this.isOpen;
    }
}