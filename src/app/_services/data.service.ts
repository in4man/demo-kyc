import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { map } from 'rxjs/operators';
import { Trust } from '@app/_models/trust';

//data.service.ts


@Injectable({providedIn: 'root'})
export class DataService {

  // - We set the initial state in BehaviorSubject's constructor
  // - Nobody outside the Store should have access to the BehaviorSubject 
  //   because it has the write rights
  // - Writing to state should be handled by specialized Store methods (ex: addTodo, removeTodo, etc)
  // - Create one BehaviorSubject per store entity, for example if you have TodoGroups
  //   create a new BehaviorSubject for it, as well as the observable$, and getters/setters
  private readonly _trust = new BehaviorSubject<Trust>(new Trust());

  // Expose the observable$ part of the _todos subject (read only stream)
  readonly trust$ = this._trust.asObservable();


  // the getter will return the last value emitted in _todos subject
  get trust(): Trust {
    return this._trust.getValue();
  }

  currentValue: Trust = new Trust();
  // assigning a value to this.todos will push it onto the observable 
  // and down to all of its subsribers (ex: this.todos = [])
  set trust(val: Trust) {
      if (this.currentValue) {
            if (val.grantor) {
                  this.currentValue.grantor = { ... val.grantor };
            }
            if (val.administrators) {
                  this.currentValue.administrators = val.administrators;
//                  this.currentValue.administrators = [ ...this.currentValue.administrators, ... val.administrators ];
            }
            if (val.beneficiaries) {
                  this.currentValue.beneficiaries = val.beneficiaries;
                   // [  ...this.currentValue.beneficiaries, ...val.beneficiaries ];
            }
            if (val.trustName) {
              this.currentValue.trustName = val.trustName;
            }
      } else {
            this.currentValue = val;
      }
      this._trust.next(this.currentValue);
  }

}
