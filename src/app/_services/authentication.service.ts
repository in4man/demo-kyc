﻿import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable, Subject, throwError } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import { of } from 'rxjs';
import {Location} from '@angular/common';

import { environment } from '@environments/environment';
import { User } from '@app/_models';
import { AlertService } from '../../app/_services/alert.service';
import { TrustStatuses } from '@app/_models/trustStatuses';
import { Router } from '@angular/router';


@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    public currentUserSubject: BehaviorSubject<User>; //private
    public currentUser: Observable<User>;
    model: { currentStatus: TrustStatuses } = { currentStatus: TrustStatuses.TrustEmpty};
    public trustStatuses: any = TrustStatuses;

    constructor(private alertService: AlertService, private http: HttpClient, private router: Router) {
      if(localStorage.getItem('currentUser') !== 'undefined') {
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
        this.currentUserSubject.next(this.currentUserValue);
      } else {
        this.httpGET( '/user', {}).subscribe( data => { 
          this.currentUserSubject = new BehaviorSubject<User>(data);
          this.currentUser = this.currentUserSubject.asObservable();
          this.currentUserSubject.next(this.currentUserValue);
        });
      }
    }

    public get currentUserValue(): User {
        return this.currentUserSubject.value;
    }

    goToExternal() {
      window.location.href = environment.acUrl + '/MyOrders';
  }

    httpRequest_XHR(method,path,dataObj,callback){
      var endpoint = environment.apiUrl;

      var httpPost = new XMLHttpRequest();

      httpPost.onload = function(err) {
        if (httpPost.readyState == 4 && httpPost.status == 200){
          var response=JSON.parse(httpPost.responseText)//here you will get uploaded image id
          callback(response);
        } else {
          console.log(err);
        }
      }
      httpPost.open(method, endpoint+path, true);
      httpPost.setRequestHeader('Content-Type', 'application/json');//Specifies type of request
      httpPost.send(JSON.stringify(dataObj))
    }

    private handleError(error: HttpErrorResponse) {
      if (error.error instanceof ErrorEvent) {
        // A client-side or network error occurred. Handle it accordingly.
        console.error('An error occurred:', error.error.message);
      } else {
        // The backend returned an unsuccessful response code.
        // The response body may contain clues as to what went wrong,
        console.error(
          `Backend returned code ${error.status}, ` +
          `body was: ${error.error}`);
      }
      // return an observable with a user-facing error message
      return throwError(
        'Something bad happened; please try again later.');
    };
    
    httpRequest(method, path, dataObj, headrs = {}): Observable<any>{
      const endpoint = environment.apiUrl;

      let options: any =  headrs; // {};
      // options = { 'requestType': 'JSON'};
      // if (headrs != null && Object.keys(headrs).length > 0) {
      //   Object.keys(headrs).forEach(header => {
      //     options[header] = headrs[header];
      //   });
      // }

      switch(method) {
        case 'POST':
            return this.http.post(endpoint + path, dataObj, options);
          break;
        case 'PUT':
            return this.http.put(endpoint + path, dataObj, options);
          break;
        default:
            return of('Request with not POST not yet supported.');
            break;
      }
      
      /*var httpPost = new XMLHttpRequest();

      httpPost.onload = function(err) {
        if (httpPost.readyState == 4 && httpPost.status == 200){
          var response=JSON.parse(httpPost.responseText)//here you will get uploaded image id
          callback(response);
        } else {
          console.log(err);
        }
      }
      httpPost.open(method, endpoint+path, true);
      httpPost.setRequestHeader('Content-Type', 'application/json');//Specifies type of request
      httpPost.send(JSON.stringify(dataObj))*/
    }

    httpGET(path,dataObj = {} /*,callback*/, headrs = {}): Observable<any>{
      const endpoint = environment.apiUrl;

      // var httpGet = new XMLHttpRequest();
     /* httpGet.onreadystatechange = ()=>{
        if (httpGet.readyState == 4 && httpGet.status == 200) {
          if(httpGet.responseType === "" && !httpGet.responseText.startsWith('{') ) {// should from backend return 'pdf' or some mime
            const response = httpGet.responseText.length > 0;
            callback(response);
          } else {
            const response = JSON.parse(httpGet.responseText);
            callback(response);
          }
        } else {
          callback(null);// throw(new Error('Some error on server'));
        }
      };*/
      let queryString = '';
      let options: any;
      if (dataObj) {
        queryString = queryString +  Object.keys(dataObj).map(function(key) {
          return key + '=' + dataObj[key];
        }).join('&');
        if (queryString !== '') {
          queryString = '?' + queryString;
          if (path.indexOf('/user/login') > -1) {
            options  = {observe: 'response', responseType: 'json' };
          } else {
            options = {};
          }
        }
      }
      if (headrs != null && Object.keys(headrs).length > 0) {
        Object.keys(headrs).forEach(header => {
          options[header] = headrs[header];
        });
      }
      // httpGet.open('GET', endpoint+path+"?"+queryString, true);
      // httpGet.send();
     /* if (headers && headers !== null) {
        options.headers = headers;
      }*/
      return this.http.get(endpoint + path + queryString, options);
      /*
      if (httpGet.status !== 200) {
        throw(new Error(httpGet.statusText));
      } */

    }

    login(email: string, password: string): Observable<any> {

        //const userObj = this.httpGET("/user/login", {email: email,password: password })
      let options: any;
      if (email.trim() !== '' && password.trim() !== '') {

        const dataObj = { email: email.trim(), password: password.trim() };

        const contentHeaders = new HttpHeaders();
        contentHeaders.set('Accept', 'application/json');
        contentHeaders.set('Content-Type', 'application/json; charset=utf-8');

        options  = {observe: 'response', responseType: 'json' , headers: contentHeaders};
        const userObj = this.httpRequest('POST', '/user/login', dataObj, options);

        return userObj;
      }
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        localStorage.removeItem('address');
        this.currentUserSubject.next(null);
      //  this.goToExternal();
    }


    // makeing user notifications:
    public alerts(): Observable<any> {
      return new Observable( obs => this.httpGET('/trust').pipe(map((response) => {
        if (response == null || response.length === 0 ) {
          return of(this.model);
        }
  
        console.log(name);
        console.log('111');
        console.log(response);
  
        for (let i = 0; i < response.length; i++) {
  
          if (response[i].trustStatus === 'submitted') {
            this.model.currentStatus = this.trustStatuses.TrustSubmitted;
          } else if (response[i].trustStatus === 'empty') {
            this.model.currentStatus = this.trustStatuses.TrustEmpty;
          } else if (response[i].trustStatus === 'rejected') {
            this.model.currentStatus = this.trustStatuses.TrustRejected;
          } else if (response[i].trustStatus === 'accepted') {
            this.model.currentStatus = this.trustStatuses.TrustAccepted;
            if (response[i].doc && response[i].doc.docStatus === 'submitted') {
              this.model.currentStatus = this.trustStatuses.DocSubmitted;
            }
          } else if (response[i].trustStatus === 'filled') {
            this.model.currentStatus = this.trustStatuses.TrustFilled;
          } if (response[i].trustStatus === 'trustRejected'){
            this.model.currentStatus = this.trustStatuses.TrustRejected;
          } else if (response[i].trustStatus === 'ucc1') {
            this.model.currentStatus = this.trustStatuses.TrustSubmitted;
            // this.alertService.success("Trust Submitted. Download you docs.");
          } else if (response[i].trustStatus === 'ucc'){
            this.alertService.warning('Trust on check, wait!');
          } else if (response[i].trustStatus === 'docRejected') {
            this.alertService.error('Incorrect Docs. Update Again!');
          } else if (response[i].trustStatus === 'docSubmitted') {
            this.alertService.success('Doc submitted!');
          }
  
        if (this.model.currentStatus !== TrustStatuses.TrustEmpty) {
            // do in concrete components please, else if critical then here
            switch(this.model.currentStatus ) {
              case TrustStatuses.TrustFilled:
                  this.alertService.success('Thank you. Your trust documents are being reviewed and prepared.' +
                  '  You will receive an email when these are ready for the next step.');
                break;
              case TrustStatuses.TrustSubmitted:
                  let thisOrNext1 = 'next';
                  if (this.router.url.indexOf('trust-info') === 1) {
                    thisOrNext1 = 'this';
                  }
                  this.alertService.success('Thank you. Your trust documents are submitted.' +
                  '  Please continue, download your docs in ' + thisOrNext1 +' step soon.');
                  break;
              case TrustStatuses.TrustRejected:
                  this.alertService.error('Trust Rejected. Please repeat!');
                  break;
              case TrustStatuses.TrustAccepted:
                let thisOrNext = 'next';
                if (this.router.url.indexOf('trust-info') === 1) {
                  thisOrNext = 'this';
                }
                  this.alertService.success('Thank you. Your trust documents are submitted.' +
                  '  Please continue, download your docs in ' + thisOrNext + ' step. Documents are ready');
                  break;
              case TrustStatuses.DocSubmitted:
                  this.alertService.success('Thank you. Your trust documents are submitted and approved.' +
                  '  All documents have been sent successfully.');
                  break;
              default:
                  break;
            }
            // this.alertService.success('Thank you. Your trust documents are being reviewed and prepared.' +
            // '  You will receive an email when these are ready for the next step.', true);
          } else {
            this.alertService.info('Welcome to the METANOMICS Trust System.  Create your METANOMICS Trust here - it can be used with META1. IO Website, META Exchange, Meta Investment Bank and META Wallet. A trust account enables additional functionality in each system!');
          }
  
        }
        return this.model;
      },
       error => {
        this.alertService.error("Some wrong");
        return throwError(of(this.model));
      })).subscribe(data =>
        obs.next(data)) );  
    }
}
