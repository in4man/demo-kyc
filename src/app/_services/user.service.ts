﻿import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from '@environments/environment';
import { User } from '@app/_models';
import { AuthenticationService } from './authentication.service';
import { Observable, throwError, timer, of } from 'rxjs';
import { tap, map, catchError } from 'rxjs/operators';
import { AlertService } from './alert.service';

@Injectable({ providedIn: 'root' })
export class UserService {
    isLoggedIn: boolean;

    setAutoUpdate() {
            timer(1000 * /*5*/ 5 * 60 /* 5 mins*/, 1000 * /*5*/ 5 * 60 /* 5 mins*/ ).subscribe( async () => {
                const token1 = this.auth.currentUserValue.token;
                this.loginByToken(environment.acUrlRenew, token1).subscribe( s => {
                    if ( s.Error !== '' &&  s.Error.toString().toLowerCase() !== 'ok') {
                        // error or token expired
                        this.auth.goToExternal();
                    }
                    }, e => {
                    this.auth.goToExternal();
                    // token expired
                    });
            });
    }
    loginByToken(acUrl: string, tempToken: any): Observable<any> {
        // const headers = new HttpHeaders( {'Content-Type': 'application/json'} );
        
        return this.http.post(environment.acUrl + environment.acUrlRenew, { token: tempToken } /*, { headers: headers }*/).pipe( tap( (s:any) => {
            /* all api */
            let token = s['Token'];
            if (s.Error !== undefined && s.Error.toString().toLowerCase() !== 'ok') {
                token = 'INVALID';
                this.alertService.error('Please try again, authentication time expired');
                throwError(s.Error);
                return;
                //return Error('Error authentication');
           }
            const user = new User();
            this.auth.currentUserSubject.next( { ...user, token} );
            if (!this.isLoggedIn) {
                this.setAutoUpdate();
                this.isLoggedIn = true;
            }
            // this.auth.currentUserValue.token = tempToken;
        },
        e => { 
            this.alertService.error('Please try again, authentication time expired');
            return Error('Error authentication');
        }),
        catchError(e => throwError(e))
        );
    }

    constructor(private http: HttpClient, private auth: AuthenticationService, private alertService: AlertService) {
        this.isLoggedIn = false;
     }

    getExtendedUser() {
        return this.http.get<any>(`${environment.apiUrl}/user/profile_extended`);
    }

    updateExtendedUser(data) {
        
        const contentHeaders = new HttpHeaders();
        contentHeaders.set('Accept', 'application/json');
        contentHeaders.set('Content-Type', 'application/json; charset=utf-8');

        const options  = {observe: 'response', responseType: 'json' , requestType: 'json', headers: contentHeaders};
    
        return this.auth.httpRequest('POST', `/user/profile_extended`, data, options); //id

    }

    getProduct(userDocType: string): Observable<any> {
        return this.http.get<any>(`${environment.apiUrl}/product?sku=${userDocType}`);
      }

    getAll() {
        return this.http.get<User[]>(`${environment.apiUrl}/users`);
    }

    getAddress(): Observable<User> {
        return this.http.get<User>(`${environment.apiUrl}/user/address`);
    }
    get(): Observable<User> {
        return this.http.get<User>(`${environment.apiUrl}/user`);
    }

    register(user: User) {
        return this.http.post(`${environment.apiUrl}/users/register`, user);
    }

    verify(user: User) {
        return this.http.post(`${environment.apiUrl}/users/verify`, user);
        console.log(User);
    }
/*
    grantor(user: User) {
        return this.http.post(`${environment.apiUrl}/users/grantor`, user);
        console.log(User);
    }

    administrator(user: User) {
        return this.http.post(`${environment.apiUrl}/users/administrator`, user);
        console.log(User);
    }
*/
    update(user: User) {
        
        const contentHeaders = new HttpHeaders();
        contentHeaders.set('Accept', 'application/json');
        contentHeaders.set('Content-Type', 'application/json; charset=utf-8');

        const options  = {observe: 'response', responseType: 'json' , requestType: 'json', headers: contentHeaders};
    
        return this.auth.httpRequest('POST', `/user/address`, user, options); //id
    }

    /*delete(id: number) {
        return this.http.delete(`${environment.apiUrl}/users/${id}`);
    }*/
}
