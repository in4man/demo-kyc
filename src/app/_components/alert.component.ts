﻿import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { AlertService } from '@app/_services';
import { SafeHtml, SafeUrl, DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

class SafeString {
    type: string;
    text: SafeHtml;
}

@Component({
    selector: 'alert',
    templateUrl: 'alert.component.html'
})
export class AlertComponent implements OnInit, OnDestroy {
    private subscription: Subscription;

    private _message: any = null;
    public get message(): SafeString {
        if (this._message) {
        return <SafeString>{ type: this._message.type, text: this.sanitizer.bypassSecurityTrustHtml(this._message.text)
            };
        } else {
            return null;
        }
    }
    public setMessage(value: any) {
        if (value) {
            this._message = {...value};
        } else if (value === null)
        {
            this._message = null;
        }
    }

    constructor(private alertService: AlertService, protected sanitizer: DomSanitizer) { }

    ngOnInit() {
        this.subscription = this.alertService.getMessage().subscribe(message => { 
            this.setMessage(message);
        });
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
}
