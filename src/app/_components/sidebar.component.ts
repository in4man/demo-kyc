import { Component, HostBinding, Input, Output, ViewChild, OnInit } from '@angular/core';
import { SideBarService } from '@app/_services/sidebar.service';
import { SafeHtml, SafeUrl } from '@angular/platform-browser';
import { AuthenticationService } from '@app/_services';
import { PdfViewerComponent, PDFDocumentProxy } from 'ng2-pdf-viewer';


@Component({
  selector: 'app-side-bar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SideBarComponent implements OnInit {

  // @HostBinding('class.is-open')
  view: {isOpen: boolean, data: SafeHtml, pdfPreview: { url: any, httpHeaders: any} };
 @ViewChild('pdfPreview', {static: true}) pdfPreview: PdfViewerComponent;
  tempPdf: any;
  fileName: string;
  @Output()
  public get isOpen(): boolean {
    return this.view !== undefined && this.view.isOpen;
  }
 // @Input() ;
  constructor(
    private sideBarService: SideBarService, private authenticationService: AuthenticationService
  ) { 
    this.view = { isOpen: false, data: '', pdfPreview: {httpHeaders: {}, url: '' } };
    authenticationService.currentUser.subscribe(user => {
      if (user === null ) {
        return;
      }
      this.view.pdfPreview.httpHeaders = { 'Authorization': user.token };

     
    });

    //init first time
    this.sideBarService.change.subscribe( (dataToShow: any) => {
      // this.view = { data: dataToShow, isOpen: true };
      if (dataToShow !== null) {
          this.view.isOpen = false;
        if (dataToShow.params !== undefined ) {
          this.fileName = dataToShow.params.fileName;
          this.view.data = dataToShow.data;
          this.view.pdfPreview.url = undefined;
          // get dynamic component for pdf
          if (dataToShow.url !== undefined && dataToShow.url !== '') {
            this.view.pdfPreview.url = dataToShow.url;
            this.view.pdfPreview.httpHeaders['Authorization'] = this.authenticationService.currentUserValue.token;
          }
        } else {
          this.view.data = dataToShow.data;
        }
        this.view.isOpen = true;
      } else {
        this.view = { isOpen: false, data: '', pdfPreview: {httpHeaders: {}, url: '' } };
      }
    });

  }

  downloadFile(blob: any, type: string, filename: string): string {
    const url = window.URL.createObjectURL(blob); // <-- work with blob directly
    this.fileName = filename;
    // create hidden dom element (so it works in all browsers)
    const a = document.createElement('a');
    a.setAttribute('style', 'display:none;');
    document.body.appendChild(a);
  
    // create file, attach to hidden element and open hidden element
    a.href = url;
    a.download = filename;
    a.click();
    return url;
  }
  download(fileNam) {
    this.tempPdf.getData().then((u8) => {
      const blob = new Blob([u8.buffer], {
          type: 'application/pdf'
      });
      if (window.navigator && window.navigator.msSaveOrOpenBlob) {
          // IE11 and Edge
          window.navigator.msSaveOrOpenBlob(blob, fileNam);
      } else {
          this.downloadFile(blob, 'application/pdf', fileNam);
          // // Chrome, Safari, Firefox, Opera
          // let url = URL.createObjectURL(blob);
          // this.openLink(url);
          // // Remove the link when done
          // setTimeout(function () {
          //     window.URL.revokeObjectURL(url);
          // }, 5000);
      }
  });
  }

  initPdf(pdf: PDFDocumentProxy) {
    this.tempPdf = pdf;
  }
  print() {
    // this.printFile(); //pdfPreview.print();
    this.tempPdf.getData().then((u8) => {
      const blob = new Blob([u8.buffer], {
          type: 'application/pdf'
      });

    const fl = URL.createObjectURL(blob);
    window.open(fl).print();
  });
}

  ngOnInit() {
  //   this.view = { isOpen: false, data: ``,
  //   pdfPreview: { url: undefined, httpHeaders: { 'Authorization': '' }
  //  } };
  this.sideBarService.change.subscribe( (dataToShow: any) => {
    // this.view = { data: dataToShow, isOpen: true };
    if (dataToShow !== null) {
        this.view.isOpen = false;
      if (dataToShow.params !== undefined ) {
        this.fileName = dataToShow.params.fileName;
        this.view.data = dataToShow.data;
        this.view.pdfPreview.url = undefined;
        // get dynamic component for pdf
        if (dataToShow.url !== undefined && dataToShow.url !== '') {
          this.view.pdfPreview.url = dataToShow.url;
        }
      } else {
        this.view.data = dataToShow.data;
      }
      this.view.isOpen = true;
    } else {
      this.view = { isOpen: false, data: '', pdfPreview: {httpHeaders: {}, url: '' } };
    }
  });
  }

}
