import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationStart, NavigationEnd } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first, timeInterval, timeout, delay, takeUntil, takeWhile, pluck, tap, take } from 'rxjs/operators';

import { User } from '@app/_models';
import { AlertService, UserService, AuthenticationService } from '@app/_services';
import { DomSanitizer } from '@angular/platform-browser';
import { interval, Observable, timer } from 'rxjs';

declare var $: any;

@Component({templateUrl: 'register.component.html'})

export class RegisterComponent implements OnInit {
    registerForm: FormGroup;
    loading = false;
    submitted = false;
    resendCompleted: boolean = null;
    intervalRemaing: number = 15;
    viewModel: any = { showResend: false };

    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private authenticationService: AuthenticationService,
        private userService: UserService,
        private alertService: AlertService,
        private route: ActivatedRoute
    ) {
        // this.router.routeReuseStrategy.shouldReuseRoute = () => true;
        // redirect to home if already logged in
        if (this.authenticationService.currentUserValue !== null) {
            this.router.navigate(['/']);
        }

        router.events.subscribe(event => {
            if (event instanceof NavigationEnd) {
                if (event.url.indexOf('/register#resendEmail') > -1) {
                    this.onResend();
                }
            }
            if (event instanceof NavigationStart) {
                if (event.navigationTrigger === 'imperative') { 
                    //hashchange
                    // if (event.url.indexOf('/register#resendEmail') > -1) {
                    //     this.onResend();
                    // } else
                    if( (this.resendCompleted === true) && event.url.indexOf('/register#resend') === 0)
                    {
                        this.submitted = true;
                        this.resendCompleted = false;
                        this.intervalRemaing = 15; 
                        this.alertService.info('Email is sending. Please wait '+this.intervalRemaing.toString()+' seconds to send again');

                        let tim: any = null;
                         timer(2500).subscribe(()=> { 
                            
                            tim = interval(this.intervalRemaing * 1000 + 1000).pipe(take(1));
                            const t2 = timer(1000, 1000).pipe(
                                takeUntil(tim)).subscribe(x => {
                                    
                                    this.alertService.infoWithLink('Email resent.'+
                                    ' Please verify your email address by clicking the link in an email sent to the email address' 
                                    +' you provided.' +
                                    '<br>Did not receive an email?  Please check spam folder or wait ' +
                                    Math.ceil(15 - x).toString() +
                                    ' seconds to send again. Click <a href="/register?#resend" >here</a> to resend.');
                                });

                                tim.subscribe(s => {
                                    if (s >= 0) {
                                        // this.onSubmit();
                                        this.resend();
                                        this.intervalRemaing = 0;
                                        this.resendCompleted = true; this.submitted = false;

                                        this.alertService.infoWithLink('Email resent.' 
                                        + ' Please verify your email address by clicking the link in an email sent to '
                                        + 'the email address you provided.' +
                                        '<br>Did not receive an email?  Please check spam folder or wait ' +
                                        this.intervalRemaing.toString() + 
                                        ' seconds to send again. Click <a href="/register?#resend" >here</a> to resend.');
                                    }
                                });
                            });

                    }
                }
            }
        });
       /*
            this.route.fragment.subscribe(fragment => { if (fragment === 'resend') {
                this.submitted = false;
                this.onSubmit();
                this.alertService.info('Email has been resent. Wait 1 sec to send again');
                setTimeout(() => { this.submitted = true; }, 500);
            } });
            this.router.events.subscribe(routerEvent => {
        });*/
        
        /*this.route.queryParamMap.subscribe(params => {
            if(params[0] === true)
            {
                this.submitted = false;
                this.onSubmit();
                this.alertService.info('Email has been resent. Wait 1 sec to send again');
                setTimeout(() => { this.submitted = true; }, 500);
            }
        });*/
    }

    ngOnInit() {

        this.registerForm = this.formBuilder.group({
            firstName: ['', Validators.required],
            lastName: ['', Validators.required],
            number: ['', [Validators.required, Validators.minLength(9), Validators.maxLength(12), Validators.pattern('[0-9]*')]],
            email: ['', [Validators.required, Validators.email]],
            password: ['', [Validators.required, Validators.minLength(8)]],
        });

    }



    // convenience getter for easy access to form fields
    public get f(): any { return this.registerForm.controls; }

    public resend() {
          // stop here if form is invalid
          if (this.registerForm.controls.email.invalid) {
            return;
        }
        this.loading = true;

        this.authenticationService.httpGET('/user/resend', {email: this.registerForm.value.email})
        .subscribe( (response) =>{
          console.log(response);
          this.loading = false;

          this.submitted = false;

          if(response && response.result !== '') {
            if (this.resendCompleted === null) {
                this.resendCompleted = true;
                this.alertService.infoWithLink(response.result +
            '<br>Did not receive an email?  Please check spam folder or click <a href="/register?#resend" >here</a> to resend.');
                } else { 
                this.resendCompleted = true;
                this.alertService.infoWithLink(response.result +
                // tslint:disable-next-line:max-line-length
                '<br>Did not receive an email?  Please check spam folder or wait '+ this.intervalRemaing + ' seconds to send again. Click <a href="/register?#resend" >here</a> to resend.');
                }
            }
            else {
                this.resendCompleted = true;
                this.alertService.infoWithLink( 'Can not resend. ' + response.error +
                '<br>Did not receive an email?  Please check spam folder or wait '+ this.intervalRemaing + ' seconds to send again. Click <a href="/register?#resend" >here</a> to resend.');
                }
            }, error => {
                    this.resendCompleted = true;
                    this.alertService.infoWithLink(error.error.error +
                    '<br>Did not receive an email?  Please check spam folder or wait '+ this.intervalRemaing + ' seconds to send again. Click <a href="/register?#resend" >here</a> to resend.');

                    this.loading = false;
            }
        );

    }

    public onResend() {
        this.viewModel.showResend = true;
    }
    public onSubmit() {
        // event: any event.preventDefault();
        // event.stopPropagation();
        this.submitted = true;

        // stop here if form is invalid
        if (this.registerForm.invalid) {
            return;
        }

        this.loading = true;
        const dataObj = this.registerForm.value;
console.log(dataObj);
        this.authenticationService.httpRequest('POST','/user',dataObj)
        .subscribe( (response)=>{
          console.log(response);
          this.loading = false;

          this.submitted = false;
          if (this.resendCompleted === null) {
              this.resendCompleted = true;
              this.alertService.infoWithLink('Please verify your email address by clicking the link in an email sent to the email address you provided.' +
          '<br>Did not receive an email?  Please check spam folder or click <a href="/register?#resend" >here</a> to resend.');
            } else { 
              this.resendCompleted = true;
              this.alertService.infoWithLink('Email resent. Please verify your email address by clicking the link in an email sent to the email address you provided.' +
              '<br>Did not receive an email?  Please check spam folder or wait '+ this.intervalRemaing + ' seconds to send again. Click <a href="/register?#resend" >here</a> to resend.'); }
          this.loading = false;
/*
          this.alertService.infoWithLink('Please verify your email address by clicking the link in an email sent to the email address you provided.' +
          '<br>Did not receive an email?  Please check spam folder or click <a (click)="resendClick()">here</a> to resend.');*/
        },
        error => {
            this.loading = false;
        
            this.alertService.info('Some registration problems. Please fix and try again: ' + error.error.error);
        });

    }
}
