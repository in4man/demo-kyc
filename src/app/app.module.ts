﻿import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule }    from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';


// used to create fake backend
import { fakeBackendProvider } from './_helpers';

import { AppComponent }  from './app.component';
import { routing }        from './app.routing';
import { RouterModule, Routes } from '@angular/router';

import { AlertComponent } from './_components';
import { JwtInterceptor, ErrorInterceptor } from './_helpers';
import { HomeComponent } from './home';
import { LoginComponent } from './login';
import { RegisterComponent } from './register';
import { StatisticsComponent } from './statistics';
import { MyPasswordComponent } from './my-password';
// import { AuthService } from '@app/_services';
import { VerifyEmailComponent } from './verify-email';;
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import {MatTabsModule} from '@angular/material/tabs';
import {MatStepperModule} from '@angular/material/stepper';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatDialogModule} from '@angular/material/dialog';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatDatepickerModule,MatNativeDateModule,MatIconModule, MatError, MatSelectModule, MatSelect, MatSidenavModule, MatListModule, MatTableModule, MatSnackBar, MatSnackBarModule, MatProgressSpinnerModule} from '@angular/material';
import {
  MatButtonModule,
  MatFormFieldModule,
  MatInputModule,
  MatRippleModule
} from '@angular/material';
import { Info1Component } from './home/info1/info1.component'
;
import { IndoForAmountComponent1 } from './home/indo-for-amount/indo-for-amount.component';
import { NextstepComponent } from './nextstep';
import { MatCheckboxModule } from '@angular/material/checkbox';
import {MatBadgeModule} from '@angular/material/badge';
import { FirststepComponent } from './firststep/firststep.component'
;
import { DocModuleComponent } from './doc-module/doc-module.component';
import {MatCardModule} from '@angular/material/card';
import { UserComponent } from './user/user.component'
import { AlertService } from './_services/alert.service';
import { DataService } from './_services/data.service';
import { Info1Component2 } from './firststep/info1/info1.component';
import { IndoForAmountComponent } from './firststep/indo-for-amount/indo-for-amount.component';
import { RouteTransformerDirective } from './_helpers/route-transformer.directive';;
import { TrustInfoComponent, OpenPopup } from './trust-info/trust-info.component'
import { SideBarService } from './_services/sidebar.service';
import { SideBarComponent } from './_components/sidebar.component';;
import { TrustInfoDetailComponent } from './trust-info-detail/trust-info-detail.component'
import { RouteStateService } from './_services/route-state.service';

import { PdfViewerModule } from 'ng2-pdf-viewer';
import { SafePipe } from './_helpers/safe.pipe';
import { UserDocModuleComponent } from './user-doc-module/user-doc-module.component';
import { TextMaskModule } from 'angular2-text-mask';

@NgModule({
    imports: [
        BrowserModule,
        ReactiveFormsModule,
        HttpClientModule,
        routing,
        RouterModule,
        BrowserAnimationsModule,
        MatTabsModule,
        MatStepperModule,
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        MatRippleModule,
        MatExpansionModule,
        MatDialogModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatIconModule,
        MatCheckboxModule,
        MatBadgeModule,
        MatTooltipModule,
        MatCardModule,
        MatSelectModule,
        MatSidenavModule,
        MatListModule,
        MatTableModule,
        MatSnackBarModule,
        PdfViewerModule,
        TextMaskModule,
        MatProgressSpinnerModule
    ],

    declarations: [
        AppComponent,
        AlertComponent,
        HomeComponent,
        LoginComponent,
        RegisterComponent,
        StatisticsComponent,
        MyPasswordComponent ,
        VerifyEmailComponent,
        Info1Component,
        Info1Component2,
        IndoForAmountComponent,
        IndoForAmountComponent1,
        NextstepComponent,
        FirststepComponent,
        SideBarComponent,
        DocModuleComponent,
        UserComponent,
        RouteTransformerDirective,
        OpenPopup,
        TrustInfoComponent
,
        TrustInfoDetailComponent,
        UserDocModuleComponent,
        SafePipe
         ],
    entryComponents: [
        Info1Component,
        IndoForAmountComponent1,
        OpenPopup
       ],
    providers: [
        MatDatepickerModule,
        MatExpansionModule,

        { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },

        // provider used to create fake backend
        // fakeBackendProvider
        AlertService,
        DataService,
        SideBarService,
        RouteStateService
    ],

    bootstrap: [AppComponent]
})

export class AppModule { }
