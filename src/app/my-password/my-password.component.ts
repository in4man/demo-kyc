import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


import { AlertService, UserService, AuthenticationService } from '@app/_services';
import { User } from '@app/_models';


@Component({
  selector: 'app-my-password',
  templateUrl: './my-password.component.html',
  styleUrls: ['./my-password.component.css']
})
export class MyPasswordComponent implements OnInit {
  
  pass: FormGroup;
  onSubmit() {
    if (this.pass.invalid) {
      return;
    }
    this.authService.httpGET('/user/reset_password', {email: this.pass.value.email}).subscribe(result => {
      if (result.result !==undefined) {
        this.alertService.info('Password reset instructions have been sent to this email.');
      }
    },
    error => {
      if (error.error !==undefined) {
        this.alertService.error('Could not send password reset instructions to this email. ' + error.error.error);
      }
    });
  }

  constructor(private _router: Router, private formBuilder: FormBuilder, private authService: AuthenticationService, private alertService: AlertService ) {
    this.pass = this.formBuilder.group(
      { 'email':['',  [Validators.required, Validators.email]] }
    );
   }
  onBack(): void {
     this._router.navigate(['/login']);
  }
  ngOnInit() {
  }

}
