import {Location} from '@angular/common';
import { Directive, ElementRef, HostListener } from '@angular/core';
import { Router } from '@angular/router';

@Directive({
  selector: '[routeTransformer]'
})
export class RouteTransformerDirective {

  constructor(private el: ElementRef, private router: Router, private location: Location) { }

  @HostListener('click', ['$event'])
  public onClick(event) {
    if (event.target.tagName === 'A') {
      this.router.navigateByUrl(this.location.path(false) + event.target.hash);
      event.preventDefault();
    } else {
      return;
    }
  }

};