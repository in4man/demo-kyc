import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Location } from '@angular/common';
import { AuthenticationService, AlertService, UserService } from '@app/_services';
import { Router } from '@angular/router';
import { User } from '@app/_models';
import { environment } from '@environments/environment';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    constructor(private authenticationService: AuthenticationService, private router: Router, private _location: Location,
        private alert: AlertService, private userService: UserService) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(catchError( (err: Observable<HttpEvent<any>>) => {
            /*if (err.status === 401) {
                // auto logout if 401 response returned from api
                this.authenticationService.logout();
                location.reload(true);
            }*/
            console.log( (err as unknown as HttpErrorResponse).error.error);
            let currentUser = this.authenticationService.currentUserValue;
            if (currentUser && currentUser.token === 'INVALID') {
                this.authenticationService.goToExternal();
            }
            // this.authenticationService.logout();
            // if ((err as unknown as HttpErrorResponse).error.error === 'invalid token') {
            //     this.alert.info('Reconnecting. Token expired');

            //     this.userService.loginByToken(environment.acUrl, this.authenticationService.currentUserValue.tempToken).subscribe( s => {
            //         this.router.navigateByUrl('/');
            //         },
            //         e => {
            //         // this.alertService.error('Please try again, authentication failed');
            //         });
            // }
            // 
            // this._location.back();
            // const error = err.error || err.error.message || err.statusText;
            return throwError(err); //throwError(of( error )); 
        }))
    }
}