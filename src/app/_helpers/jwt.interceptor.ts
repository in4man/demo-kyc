import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpResponse } from '@angular/common/http';
import { Observable, of, throwError, EMPTY } from 'rxjs';

import { map, tap, catchError } from 'rxjs/operators';
import { AuthenticationService } from '@app/_services';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    constructor(private authenticationService: AuthenticationService) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // add authorization header with jwt token if available
        let currentUser = this.authenticationService.currentUserValue;
        if (currentUser && currentUser.token !== 'INVALID') {
            request = request.clone({
                headers: request.headers.set('Authorization', `${currentUser.token}`) // 'Bearer '
                }
            );
        } else if (currentUser && currentUser.token === 'INVALID') {
            this.authenticationService.logout();
            return EMPTY;
        }

        // if (request.url.indexOf('acApi') >- 1) {
        //     request = request.clone({
        //         url: request.url.replace('//', 'http://')
        //         }
        //     );
        // }
        return next.handle(request);
        /*.pipe(tap(
            event => {
                return of(event);
            }*/
            /*,
        error => {
            return cat(val => { return of(error) });
        },
        () => {

        }*/
        /*))*/;
    }
}